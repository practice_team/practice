#pragma once

#include <iostream>
#include <vector>
#include <algorithm>

template <class T> class Binomial {
protected:
	const double eps = 0.00000000001;
	const double TwoPi = 6.28318530717958648;
	const double SolveEps = 1e-14;

	std::vector <T> coeff;
	Binomial <T>&   minus_offset(const Binomial <T> &);
	void            balance();

public:
	//Constructors
	Binomial(T x0 = 0);

	Binomial(T, T);

	Binomial(T, T, T);

	Binomial(T *arg, unsigned count);

	//Methods
	T            value(T)  const;
	unsigned int degree()  const;
	int          solve(T*) const;
	unsigned int size()    const;

	//Operators

	T&         operator [] (const unsigned);
	T          operator [] (const unsigned) const;

	Binomial& operator =  (const Binomial&);
	Binomial& operator =  (const T&       );

	Binomial& operator += (const Binomial&);
	Binomial& operator += (const T&       );

	Binomial& operator -= (const Binomial&);
	Binomial& operator -= (const T&       );

	Binomial& operator *= (const Binomial&);
	Binomial& operator *= (const T&       );


	template <class T> friend Binomial       operator -  (const Binomial&               );
	template <class T> friend std::ostream&  operator << (std::ostream&, const Binomial&);
	template <class T> friend std::istream&  operator >> (std::istream&, const Binomial&);

	template <class T> friend Binomial      operator +  (const Binomial&, const Binomial&);
	template <class T> friend Binomial      operator +  (const Binomial&, const T&       );
	template <class T> friend Binomial      operator +  (const T&       , const Binomial&);

	template <class T> friend Binomial      operator -  (const Binomial&, const Binomial&);
	template <class T> friend Binomial      operator -  (const Binomial&, const T&       );
	template <class T> friend Binomial      operator -  (const T&       , const Binomial&);

	template <class T> friend Binomial      operator *  (const Binomial&, const Binomial&);
	template <class T> friend Binomial      operator *  (const Binomial&, const T&       );
	template <class T> friend Binomial      operator *  (const T&       , const Binomial&);
};