﻿#pragma once

#include "AboutDlg.h"
#include "Instruction.h"
#include "binomial.cpp"
#include "trinomial.cpp"

#include "matrix.cpp"
#include "qmatrix.cpp"
#include "smatrix.cpp"
#include "slau.cpp"

#include <iostream>
#include <windows.h>

System::String^ wchar_tToSysString(const wchar_t* ch) {
	const wchar_t * chr = ch;
	System::String^ str;

	for (int i = 0; chr[i] != L'\0'; i++)
	{
		str += ch[i];
	}

	return str;
}

namespace Practice {
	const unsigned START_WIDTH = 475;
	const unsigned START_HEIGHT = 125;
	const unsigned SOLVE_WIDTH = 500;
	const unsigned SOLVE_HEIGHT = 500;
	const unsigned PRECISION = 2;

	double prevPosX = 250;
	double prevPosY = 250;

	double OV1X, OV1Y, OV2X, OV2Y;

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Windows::Forms::DataVisualization::Charting;

	/// <summary>
	/// Summary for MainWnd
	/// </summary>
	public ref class MainWnd : public System::Windows::Forms::Form
	{
	public:
		MainWnd(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainWnd()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitAltF4ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::Button^  solve;
	private: System::Windows::Forms::Label^  own_val_label;
	private: System::Windows::Forms::TextBox^  ownVal2;
	private: System::Windows::Forms::TextBox^  ownVal1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  temp1_00;
	private: System::Windows::Forms::TextBox^  temp1_10;			 
	private: System::Windows::Forms::TextBox^  temp1_01;
	private: System::Windows::Forms::TextBox^  temp1_11;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox5;
	private: System::Windows::Forms::Label^  right_arrow1_label;
	private: System::Windows::Forms::PictureBox^  pictureBox8;
	private: System::Windows::Forms::PictureBox^  pictureBox9;
	private: System::Windows::Forms::TextBox^  stepMatr1_00;
	private: System::Windows::Forms::TextBox^  stepMatr1_10;
	private: System::Windows::Forms::TextBox^  stepMatr1_01;
	private: System::Windows::Forms::TextBox^  stepMatr1_11;
	private: System::Windows::Forms::Label^  e1_label;
	private: System::Windows::Forms::PictureBox^  pictureBox10;
	private: System::Windows::Forms::PictureBox^  pictureBox11;
	private: System::Windows::Forms::TextBox^  ownVect1_1;
	private: System::Windows::Forms::TextBox^  ownVect1_2;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::PictureBox^  pictureBox13;
	private: System::Windows::Forms::PictureBox^  pictureBox16;
	private: System::Windows::Forms::PictureBox^  pictureBox17;
	private: System::Windows::Forms::PictureBox^  pictureBox19;
	private: System::Windows::Forms::PictureBox^  pictureBox20;
	private: System::Windows::Forms::TextBox^  temp2_00;
	private: System::Windows::Forms::TextBox^  stepMatr2_00;
	private: System::Windows::Forms::TextBox^  temp2_10;
	private: System::Windows::Forms::TextBox^  ownVect2_1;
	private: System::Windows::Forms::TextBox^  ownVect2_2;
	private: System::Windows::Forms::TextBox^  temp2_01;
	private: System::Windows::Forms::TextBox^  stepMatr2_10;
	private: System::Windows::Forms::TextBox^  temp2_11;
	private: System::Windows::Forms::TextBox^  stepMatr2_01;
	private: System::Windows::Forms::TextBox^  stepMatr2_11;
	private: System::Windows::Forms::Label^  right_arrow2_label;
	private: System::Windows::Forms::Label^  e2_2_label;
	private: System::Windows::Forms::PictureBox^  pictureBox21;
	private: System::Windows::Forms::Label^  quadriqMatrix_label;
	private: System::Windows::Forms::PictureBox^  pictureBox22;
	private: System::Windows::Forms::PictureBox^  pictureBox23;
	private: System::Windows::Forms::TextBox^  qFormMatr00;
	private: System::Windows::Forms::TextBox^  qFormMatr10;
	private: System::Windows::Forms::TextBox^  qFormMatr01;
	private: System::Windows::Forms::TextBox^  qFormMatr11;
	private: System::Windows::Forms::Label^  A_equal_label;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  charact_eq_label;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::TextBox^  textBox9;
	private: System::Windows::Forms::TextBox^  textBox10;
	private: System::Windows::Forms::Label^  lambda1;
	private: System::Windows::Forms::Label^  lambda;
	private: System::Windows::Forms::Label^  lambda2;
	private: System::Windows::Forms::TextBox^  ownVal;
	private: System::Windows::Forms::PictureBox^  pictureBox6;
	private: System::Windows::Forms::TextBox^  textBox11;
	private: System::Windows::Forms::TextBox^  textBox12;
	private: System::Windows::Forms::Label^  e2_1_label;
	private: System::Windows::Forms::PictureBox^  pictureBox7;
	private: System::Windows::Forms::TextBox^  charactEq;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  own_vect_label;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  ownValue2;
	private: System::Windows::Forms::TextBox^  ownValue1;


private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart1;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::Label^  label10;
private: System::Windows::Forms::TextBox^  curveType;
private: System::Windows::Forms::Button^  example;
private: System::Windows::Forms::Panel^  canonical_equation;

private: System::Windows::Forms::Label^  sign;
private: System::Windows::Forms::Label^  right_part;
private: System::Windows::Forms::Label^  b2;
private: System::Windows::Forms::Label^  a2;
private: System::Windows::Forms::PictureBox^  bar2;

private: System::Windows::Forms::PictureBox^  bar1;

private: System::Windows::Forms::PictureBox^  pictureBox14;
private: System::Windows::Forms::PictureBox^  pictureBox12;
private: System::Windows::Forms::Label^  label6;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MainWnd::typeid));
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitAltF4ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->solve = (gcnew System::Windows::Forms::Button());
			this->own_val_label = (gcnew System::Windows::Forms::Label());
			this->ownVal2 = (gcnew System::Windows::Forms::TextBox());
			this->ownVal1 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->temp1_00 = (gcnew System::Windows::Forms::TextBox());
			this->temp1_10 = (gcnew System::Windows::Forms::TextBox());
			this->temp1_01 = (gcnew System::Windows::Forms::TextBox());
			this->temp1_11 = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox5 = (gcnew System::Windows::Forms::PictureBox());
			this->right_arrow1_label = (gcnew System::Windows::Forms::Label());
			this->pictureBox8 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox9 = (gcnew System::Windows::Forms::PictureBox());
			this->stepMatr1_00 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr1_10 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr1_01 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr1_11 = (gcnew System::Windows::Forms::TextBox());
			this->e1_label = (gcnew System::Windows::Forms::Label());
			this->pictureBox10 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox11 = (gcnew System::Windows::Forms::PictureBox());
			this->ownVect1_1 = (gcnew System::Windows::Forms::TextBox());
			this->ownVect1_2 = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->pictureBox13 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox16 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox17 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox19 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox20 = (gcnew System::Windows::Forms::PictureBox());
			this->temp2_00 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr2_00 = (gcnew System::Windows::Forms::TextBox());
			this->temp2_10 = (gcnew System::Windows::Forms::TextBox());
			this->ownVect2_1 = (gcnew System::Windows::Forms::TextBox());
			this->ownVect2_2 = (gcnew System::Windows::Forms::TextBox());
			this->temp2_01 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr2_10 = (gcnew System::Windows::Forms::TextBox());
			this->temp2_11 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr2_01 = (gcnew System::Windows::Forms::TextBox());
			this->stepMatr2_11 = (gcnew System::Windows::Forms::TextBox());
			this->right_arrow2_label = (gcnew System::Windows::Forms::Label());
			this->e2_2_label = (gcnew System::Windows::Forms::Label());
			this->pictureBox21 = (gcnew System::Windows::Forms::PictureBox());
			this->quadriqMatrix_label = (gcnew System::Windows::Forms::Label());
			this->pictureBox22 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox23 = (gcnew System::Windows::Forms::PictureBox());
			this->qFormMatr00 = (gcnew System::Windows::Forms::TextBox());
			this->qFormMatr10 = (gcnew System::Windows::Forms::TextBox());
			this->qFormMatr01 = (gcnew System::Windows::Forms::TextBox());
			this->qFormMatr11 = (gcnew System::Windows::Forms::TextBox());
			this->A_equal_label = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->charact_eq_label = (gcnew System::Windows::Forms::Label());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->lambda1 = (gcnew System::Windows::Forms::Label());
			this->lambda = (gcnew System::Windows::Forms::Label());
			this->lambda2 = (gcnew System::Windows::Forms::Label());
			this->ownVal = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox6 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->e2_1_label = (gcnew System::Windows::Forms::Label());
			this->pictureBox7 = (gcnew System::Windows::Forms::PictureBox());
			this->charactEq = (gcnew System::Windows::Forms::TextBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->own_vect_label = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->ownValue2 = (gcnew System::Windows::Forms::TextBox());
			this->ownValue1 = (gcnew System::Windows::Forms::TextBox());
			this->chart1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->curveType = (gcnew System::Windows::Forms::TextBox());
			this->example = (gcnew System::Windows::Forms::Button());
			this->canonical_equation = (gcnew System::Windows::Forms::Panel());
			this->sign = (gcnew System::Windows::Forms::Label());
			this->right_part = (gcnew System::Windows::Forms::Label());
			this->b2 = (gcnew System::Windows::Forms::Label());
			this->a2 = (gcnew System::Windows::Forms::Label());
			this->bar2 = (gcnew System::Windows::Forms::PictureBox());
			this->bar1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox14 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox12 = (gcnew System::Windows::Forms::PictureBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox10))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox11))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox13))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox16))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox17))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox19))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox20))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox21))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox22))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox23))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->BeginInit();
			this->canonical_equation->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bar2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bar1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox14))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox12))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->fileToolStripMenuItem,
					this->helpToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(674, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitAltF4ToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitAltF4ToolStripMenuItem
			// 
			this->exitAltF4ToolStripMenuItem->Name = L"exitAltF4ToolStripMenuItem";
			this->exitAltF4ToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->exitAltF4ToolStripMenuItem->Text = L"&Exit Alt + F4";
			this->exitAltF4ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWnd::exitAltF4ToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->aboutToolStripMenuItem });
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(122, 22);
			this->aboutToolStripMenuItem->Text = L"&About F1";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWnd::aboutToolStripMenuItem_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(14, 56);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(453, 24);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(3, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(249, 24);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Коэффициенты уравнения";
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox1->Location = System::Drawing::Point(10, 54);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(40, 26);
			this->textBox1->TabIndex = 2;
			this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBox1->Enter += gcnew System::EventHandler(this, &MainWnd::textBox1_Enter);
			this->textBox1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox2->Location = System::Drawing::Point(89, 54);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(40, 26);
			this->textBox2->TabIndex = 3;
			this->textBox2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBox2->Enter += gcnew System::EventHandler(this, &MainWnd::textBox2_Enter);
			this->textBox2->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			// 
			// textBox3
			// 
			this->textBox3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox3->Location = System::Drawing::Point(168, 54);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(40, 26);
			this->textBox3->TabIndex = 4;
			this->textBox3->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBox3->Enter += gcnew System::EventHandler(this, &MainWnd::textBox3_Enter);
			this->textBox3->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			// 
			// textBox4
			// 
			this->textBox4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox4->Location = System::Drawing::Point(250, 54);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(40, 26);
			this->textBox4->TabIndex = 5;
			this->textBox4->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBox4->Enter += gcnew System::EventHandler(this, &MainWnd::textBox4_Enter);
			this->textBox4->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			// 
			// textBox5
			// 
			this->textBox5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox5->Location = System::Drawing::Point(321, 54);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(40, 26);
			this->textBox5->TabIndex = 6;
			this->textBox5->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBox5->Enter += gcnew System::EventHandler(this, &MainWnd::textBox5_Enter);
			this->textBox5->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			// 
			// textBox6
			// 
			this->textBox6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox6->Location = System::Drawing::Point(393, 54);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(40, 26);
			this->textBox6->TabIndex = 7;
			this->textBox6->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBox6->Enter += gcnew System::EventHandler(this, &MainWnd::textBox6_Enter);
			this->textBox6->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			// 
			// solve
			// 
			this->solve->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->solve->Location = System::Drawing::Point(278, 90);
			this->solve->Name = L"solve";
			this->solve->Size = System::Drawing::Size(90, 30);
			this->solve->TabIndex = 8;
			this->solve->Text = L"Решить";
			this->solve->UseVisualStyleBackColor = true;
			this->solve->Click += gcnew System::EventHandler(this, &MainWnd::solve_Click);
			// 
			// own_val_label
			// 
			this->own_val_label->AutoSize = true;
			this->own_val_label->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->own_val_label->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(42)), static_cast<System::Int32>(static_cast<System::Byte>(87)),
				static_cast<System::Int32>(static_cast<System::Byte>(154)));
			this->own_val_label->Location = System::Drawing::Point(3, 354);
			this->own_val_label->Name = L"own_val_label";
			this->own_val_label->Size = System::Drawing::Size(230, 22);
			this->own_val_label->TabIndex = 9;
			this->own_val_label->Text = L"Собственные значения";
			this->own_val_label->Visible = false;
			// 
			// ownVal2
			// 
			this->ownVal2->BackColor = System::Drawing::Color::White;
			this->ownVal2->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownVal2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVal2->Location = System::Drawing::Point(105, 683);
			this->ownVal2->Name = L"ownVal2";
			this->ownVal2->ReadOnly = true;
			this->ownVal2->Size = System::Drawing::Size(53, 19);
			this->ownVal2->TabIndex = 10;
			this->ownVal2->Visible = false;
			// 
			// ownVal1
			// 
			this->ownVal1->BackColor = System::Drawing::Color::White;
			this->ownVal1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownVal1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVal1->Location = System::Drawing::Point(103, 467);
			this->ownVal1->Name = L"ownVal1";
			this->ownVal1->ReadOnly = true;
			this->ownVal1->Size = System::Drawing::Size(44, 19);
			this->ownVal1->TabIndex = 10;
			this->ownVal1->Visible = false;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(56, 525);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(68, 24);
			this->label3->TabIndex = 11;
			this->label3->Text = L"A-λE =";
			this->label3->Visible = false;
			// 
			// temp1_00
			// 
			this->temp1_00->BackColor = System::Drawing::Color::White;
			this->temp1_00->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp1_00->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp1_00->Location = System::Drawing::Point(134, 503);
			this->temp1_00->Name = L"temp1_00";
			this->temp1_00->ReadOnly = true;
			this->temp1_00->Size = System::Drawing::Size(53, 19);
			this->temp1_00->TabIndex = 13;
			this->temp1_00->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp1_00->Visible = false;
			// 
			// temp1_10
			// 
			this->temp1_10->BackColor = System::Drawing::Color::White;
			this->temp1_10->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp1_10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp1_10->Location = System::Drawing::Point(134, 547);
			this->temp1_10->Name = L"temp1_10";
			this->temp1_10->ReadOnly = true;
			this->temp1_10->Size = System::Drawing::Size(53, 19);
			this->temp1_10->TabIndex = 13;
			this->temp1_10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp1_10->Visible = false;
			// 
			// temp1_01
			// 
			this->temp1_01->BackColor = System::Drawing::Color::White;
			this->temp1_01->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp1_01->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp1_01->Location = System::Drawing::Point(191, 503);
			this->temp1_01->Name = L"temp1_01";
			this->temp1_01->ReadOnly = true;
			this->temp1_01->Size = System::Drawing::Size(53, 19);
			this->temp1_01->TabIndex = 13;
			this->temp1_01->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp1_01->Visible = false;
			// 
			// temp1_11
			// 
			this->temp1_11->BackColor = System::Drawing::Color::White;
			this->temp1_11->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp1_11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp1_11->Location = System::Drawing::Point(191, 547);
			this->temp1_11->Name = L"temp1_11";
			this->temp1_11->ReadOnly = true;
			this->temp1_11->Size = System::Drawing::Size(53, 19);
			this->temp1_11->TabIndex = 13;
			this->temp1_11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp1_11->Visible = false;
			// 
			// pictureBox3
			// 
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->Location = System::Drawing::Point(129, 503);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(5, 70);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox3->TabIndex = 15;
			this->pictureBox3->TabStop = false;
			this->pictureBox3->Visible = false;
			// 
			// pictureBox5
			// 
			this->pictureBox5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox5.Image")));
			this->pictureBox5->Location = System::Drawing::Point(244, 503);
			this->pictureBox5->Name = L"pictureBox5";
			this->pictureBox5->Size = System::Drawing::Size(5, 70);
			this->pictureBox5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox5->TabIndex = 15;
			this->pictureBox5->TabStop = false;
			this->pictureBox5->Visible = false;
			// 
			// right_arrow1_label
			// 
			this->right_arrow1_label->AutoSize = true;
			this->right_arrow1_label->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->right_arrow1_label->Location = System::Drawing::Point(252, 518);
			this->right_arrow1_label->Name = L"right_arrow1_label";
			this->right_arrow1_label->Size = System::Drawing::Size(49, 37);
			this->right_arrow1_label->TabIndex = 16;
			this->right_arrow1_label->Text = L"→";
			this->right_arrow1_label->Visible = false;
			// 
			// pictureBox8
			// 
			this->pictureBox8->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox8.Image")));
			this->pictureBox8->Location = System::Drawing::Point(304, 503);
			this->pictureBox8->Name = L"pictureBox8";
			this->pictureBox8->Size = System::Drawing::Size(5, 70);
			this->pictureBox8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox8->TabIndex = 15;
			this->pictureBox8->TabStop = false;
			this->pictureBox8->Visible = false;
			// 
			// pictureBox9
			// 
			this->pictureBox9->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox9.Image")));
			this->pictureBox9->Location = System::Drawing::Point(419, 503);
			this->pictureBox9->Name = L"pictureBox9";
			this->pictureBox9->Size = System::Drawing::Size(5, 70);
			this->pictureBox9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox9->TabIndex = 15;
			this->pictureBox9->TabStop = false;
			this->pictureBox9->Visible = false;
			// 
			// stepMatr1_00
			// 
			this->stepMatr1_00->BackColor = System::Drawing::Color::White;
			this->stepMatr1_00->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr1_00->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr1_00->Location = System::Drawing::Point(309, 503);
			this->stepMatr1_00->Name = L"stepMatr1_00";
			this->stepMatr1_00->ReadOnly = true;
			this->stepMatr1_00->Size = System::Drawing::Size(53, 19);
			this->stepMatr1_00->TabIndex = 13;
			this->stepMatr1_00->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr1_00->Visible = false;
			// 
			// stepMatr1_10
			// 
			this->stepMatr1_10->BackColor = System::Drawing::Color::White;
			this->stepMatr1_10->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr1_10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr1_10->Location = System::Drawing::Point(309, 547);
			this->stepMatr1_10->Name = L"stepMatr1_10";
			this->stepMatr1_10->ReadOnly = true;
			this->stepMatr1_10->Size = System::Drawing::Size(53, 19);
			this->stepMatr1_10->TabIndex = 13;
			this->stepMatr1_10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr1_10->Visible = false;
			// 
			// stepMatr1_01
			// 
			this->stepMatr1_01->BackColor = System::Drawing::Color::White;
			this->stepMatr1_01->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr1_01->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr1_01->Location = System::Drawing::Point(366, 503);
			this->stepMatr1_01->Name = L"stepMatr1_01";
			this->stepMatr1_01->ReadOnly = true;
			this->stepMatr1_01->Size = System::Drawing::Size(53, 19);
			this->stepMatr1_01->TabIndex = 13;
			this->stepMatr1_01->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr1_01->Visible = false;
			// 
			// stepMatr1_11
			// 
			this->stepMatr1_11->BackColor = System::Drawing::Color::White;
			this->stepMatr1_11->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr1_11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr1_11->Location = System::Drawing::Point(366, 547);
			this->stepMatr1_11->Name = L"stepMatr1_11";
			this->stepMatr1_11->ReadOnly = true;
			this->stepMatr1_11->Size = System::Drawing::Size(53, 19);
			this->stepMatr1_11->TabIndex = 13;
			this->stepMatr1_11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr1_11->Visible = false;
			// 
			// e1_label
			// 
			this->e1_label->AutoSize = true;
			this->e1_label->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->e1_label->Location = System::Drawing::Point(56, 614);
			this->e1_label->Name = L"e1_label";
			this->e1_label->Size = System::Drawing::Size(41, 24);
			this->e1_label->TabIndex = 17;
			this->e1_label->Text = L"ā₁ =";
			this->e1_label->Visible = false;
			// 
			// pictureBox10
			// 
			this->pictureBox10->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox10.Image")));
			this->pictureBox10->Location = System::Drawing::Point(101, 592);
			this->pictureBox10->Name = L"pictureBox10";
			this->pictureBox10->Size = System::Drawing::Size(5, 70);
			this->pictureBox10->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox10->TabIndex = 15;
			this->pictureBox10->TabStop = false;
			this->pictureBox10->Visible = false;
			// 
			// pictureBox11
			// 
			this->pictureBox11->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox11.Image")));
			this->pictureBox11->Location = System::Drawing::Point(159, 592);
			this->pictureBox11->Name = L"pictureBox11";
			this->pictureBox11->Size = System::Drawing::Size(5, 70);
			this->pictureBox11->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox11->TabIndex = 15;
			this->pictureBox11->TabStop = false;
			this->pictureBox11->Visible = false;
			// 
			// ownVect1_1
			// 
			this->ownVect1_1->BackColor = System::Drawing::Color::White;
			this->ownVect1_1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownVect1_1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVect1_1->Location = System::Drawing::Point(106, 592);
			this->ownVect1_1->Name = L"ownVect1_1";
			this->ownVect1_1->ReadOnly = true;
			this->ownVect1_1->Size = System::Drawing::Size(53, 19);
			this->ownVect1_1->TabIndex = 13;
			this->ownVect1_1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->ownVect1_1->Visible = false;
			// 
			// ownVect1_2
			// 
			this->ownVect1_2->BackColor = System::Drawing::Color::White;
			this->ownVect1_2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVect1_2->Location = System::Drawing::Point(106, 636);
			this->ownVect1_2->Name = L"ownVect1_2";
			this->ownVect1_2->Size = System::Drawing::Size(53, 26);
			this->ownVect1_2->TabIndex = 13;
			this->ownVect1_2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->ownVect1_2->Visible = false;
			this->ownVect1_2->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::ownVect1_2_KeyUp);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label7->Location = System::Drawing::Point(56, 748);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(68, 24);
			this->label7->TabIndex = 11;
			this->label7->Text = L"A-λE =";
			this->label7->Visible = false;
			// 
			// pictureBox13
			// 
			this->pictureBox13->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox13.Image")));
			this->pictureBox13->Location = System::Drawing::Point(129, 725);
			this->pictureBox13->Name = L"pictureBox13";
			this->pictureBox13->Size = System::Drawing::Size(5, 70);
			this->pictureBox13->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox13->TabIndex = 15;
			this->pictureBox13->TabStop = false;
			this->pictureBox13->Visible = false;
			// 
			// pictureBox16
			// 
			this->pictureBox16->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox16.Image")));
			this->pictureBox16->Location = System::Drawing::Point(244, 725);
			this->pictureBox16->Name = L"pictureBox16";
			this->pictureBox16->Size = System::Drawing::Size(5, 70);
			this->pictureBox16->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox16->TabIndex = 15;
			this->pictureBox16->TabStop = false;
			this->pictureBox16->Visible = false;
			// 
			// pictureBox17
			// 
			this->pictureBox17->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox17.Image")));
			this->pictureBox17->Location = System::Drawing::Point(304, 725);
			this->pictureBox17->Name = L"pictureBox17";
			this->pictureBox17->Size = System::Drawing::Size(5, 70);
			this->pictureBox17->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox17->TabIndex = 15;
			this->pictureBox17->TabStop = false;
			this->pictureBox17->Visible = false;
			// 
			// pictureBox19
			// 
			this->pictureBox19->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox19.Image")));
			this->pictureBox19->Location = System::Drawing::Point(159, 814);
			this->pictureBox19->Name = L"pictureBox19";
			this->pictureBox19->Size = System::Drawing::Size(5, 70);
			this->pictureBox19->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox19->TabIndex = 15;
			this->pictureBox19->TabStop = false;
			this->pictureBox19->Visible = false;
			// 
			// pictureBox20
			// 
			this->pictureBox20->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox20.Image")));
			this->pictureBox20->Location = System::Drawing::Point(419, 725);
			this->pictureBox20->Name = L"pictureBox20";
			this->pictureBox20->Size = System::Drawing::Size(5, 70);
			this->pictureBox20->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox20->TabIndex = 15;
			this->pictureBox20->TabStop = false;
			this->pictureBox20->Visible = false;
			// 
			// temp2_00
			// 
			this->temp2_00->BackColor = System::Drawing::Color::White;
			this->temp2_00->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp2_00->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp2_00->Location = System::Drawing::Point(134, 725);
			this->temp2_00->Name = L"temp2_00";
			this->temp2_00->ReadOnly = true;
			this->temp2_00->Size = System::Drawing::Size(53, 19);
			this->temp2_00->TabIndex = 13;
			this->temp2_00->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp2_00->Visible = false;
			// 
			// stepMatr2_00
			// 
			this->stepMatr2_00->BackColor = System::Drawing::Color::White;
			this->stepMatr2_00->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr2_00->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr2_00->Location = System::Drawing::Point(309, 725);
			this->stepMatr2_00->Name = L"stepMatr2_00";
			this->stepMatr2_00->ReadOnly = true;
			this->stepMatr2_00->Size = System::Drawing::Size(53, 19);
			this->stepMatr2_00->TabIndex = 13;
			this->stepMatr2_00->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr2_00->Visible = false;
			// 
			// temp2_10
			// 
			this->temp2_10->BackColor = System::Drawing::Color::White;
			this->temp2_10->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp2_10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp2_10->Location = System::Drawing::Point(134, 769);
			this->temp2_10->Name = L"temp2_10";
			this->temp2_10->ReadOnly = true;
			this->temp2_10->Size = System::Drawing::Size(53, 19);
			this->temp2_10->TabIndex = 13;
			this->temp2_10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp2_10->Visible = false;
			// 
			// ownVect2_1
			// 
			this->ownVect2_1->BackColor = System::Drawing::Color::White;
			this->ownVect2_1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownVect2_1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVect2_1->Location = System::Drawing::Point(106, 814);
			this->ownVect2_1->Name = L"ownVect2_1";
			this->ownVect2_1->ReadOnly = true;
			this->ownVect2_1->Size = System::Drawing::Size(53, 19);
			this->ownVect2_1->TabIndex = 13;
			this->ownVect2_1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->ownVect2_1->Visible = false;
			// 
			// ownVect2_2
			// 
			this->ownVect2_2->BackColor = System::Drawing::Color::White;
			this->ownVect2_2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVect2_2->Location = System::Drawing::Point(106, 855);
			this->ownVect2_2->Name = L"ownVect2_2";
			this->ownVect2_2->Size = System::Drawing::Size(53, 26);
			this->ownVect2_2->TabIndex = 13;
			this->ownVect2_2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->ownVect2_2->Visible = false;
			this->ownVect2_2->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::ownVect2_2_KeyUp);
			// 
			// temp2_01
			// 
			this->temp2_01->BackColor = System::Drawing::Color::White;
			this->temp2_01->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp2_01->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp2_01->Location = System::Drawing::Point(191, 725);
			this->temp2_01->Name = L"temp2_01";
			this->temp2_01->ReadOnly = true;
			this->temp2_01->Size = System::Drawing::Size(53, 19);
			this->temp2_01->TabIndex = 13;
			this->temp2_01->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp2_01->Visible = false;
			// 
			// stepMatr2_10
			// 
			this->stepMatr2_10->BackColor = System::Drawing::Color::White;
			this->stepMatr2_10->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr2_10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr2_10->Location = System::Drawing::Point(309, 769);
			this->stepMatr2_10->Name = L"stepMatr2_10";
			this->stepMatr2_10->ReadOnly = true;
			this->stepMatr2_10->Size = System::Drawing::Size(53, 19);
			this->stepMatr2_10->TabIndex = 13;
			this->stepMatr2_10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr2_10->Visible = false;
			// 
			// temp2_11
			// 
			this->temp2_11->BackColor = System::Drawing::Color::White;
			this->temp2_11->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->temp2_11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->temp2_11->Location = System::Drawing::Point(191, 769);
			this->temp2_11->Name = L"temp2_11";
			this->temp2_11->ReadOnly = true;
			this->temp2_11->Size = System::Drawing::Size(53, 19);
			this->temp2_11->TabIndex = 13;
			this->temp2_11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->temp2_11->Visible = false;
			// 
			// stepMatr2_01
			// 
			this->stepMatr2_01->BackColor = System::Drawing::Color::White;
			this->stepMatr2_01->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr2_01->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr2_01->Location = System::Drawing::Point(366, 725);
			this->stepMatr2_01->Name = L"stepMatr2_01";
			this->stepMatr2_01->ReadOnly = true;
			this->stepMatr2_01->Size = System::Drawing::Size(53, 19);
			this->stepMatr2_01->TabIndex = 13;
			this->stepMatr2_01->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr2_01->Visible = false;
			// 
			// stepMatr2_11
			// 
			this->stepMatr2_11->BackColor = System::Drawing::Color::White;
			this->stepMatr2_11->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->stepMatr2_11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->stepMatr2_11->Location = System::Drawing::Point(366, 769);
			this->stepMatr2_11->Name = L"stepMatr2_11";
			this->stepMatr2_11->ReadOnly = true;
			this->stepMatr2_11->Size = System::Drawing::Size(53, 19);
			this->stepMatr2_11->TabIndex = 13;
			this->stepMatr2_11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->stepMatr2_11->Visible = false;
			// 
			// right_arrow2_label
			// 
			this->right_arrow2_label->AutoSize = true;
			this->right_arrow2_label->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->right_arrow2_label->Location = System::Drawing::Point(252, 740);
			this->right_arrow2_label->Name = L"right_arrow2_label";
			this->right_arrow2_label->Size = System::Drawing::Size(49, 37);
			this->right_arrow2_label->TabIndex = 16;
			this->right_arrow2_label->Text = L"→";
			this->right_arrow2_label->Visible = false;
			// 
			// e2_2_label
			// 
			this->e2_2_label->AutoSize = true;
			this->e2_2_label->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->e2_2_label->Location = System::Drawing::Point(56, 835);
			this->e2_2_label->Name = L"e2_2_label";
			this->e2_2_label->Size = System::Drawing::Size(41, 24);
			this->e2_2_label->TabIndex = 17;
			this->e2_2_label->Text = L"ā₂ =";
			this->e2_2_label->Visible = false;
			// 
			// pictureBox21
			// 
			this->pictureBox21->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox21.Image")));
			this->pictureBox21->Location = System::Drawing::Point(101, 814);
			this->pictureBox21->Name = L"pictureBox21";
			this->pictureBox21->Size = System::Drawing::Size(5, 70);
			this->pictureBox21->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox21->TabIndex = 15;
			this->pictureBox21->TabStop = false;
			this->pictureBox21->Visible = false;
			// 
			// quadriqMatrix_label
			// 
			this->quadriqMatrix_label->AutoSize = true;
			this->quadriqMatrix_label->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->quadriqMatrix_label->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(42)),
				static_cast<System::Int32>(static_cast<System::Byte>(87)), static_cast<System::Int32>(static_cast<System::Byte>(154)));
			this->quadriqMatrix_label->Location = System::Drawing::Point(3, 120);
			this->quadriqMatrix_label->Name = L"quadriqMatrix_label";
			this->quadriqMatrix_label->Size = System::Drawing::Size(296, 22);
			this->quadriqMatrix_label->TabIndex = 18;
			this->quadriqMatrix_label->Text = L"Матрица квадратичной формы";
			this->quadriqMatrix_label->Visible = false;
			// 
			// pictureBox22
			// 
			this->pictureBox22->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox22.Image")));
			this->pictureBox22->Location = System::Drawing::Point(66, 153);
			this->pictureBox22->Name = L"pictureBox22";
			this->pictureBox22->Size = System::Drawing::Size(5, 70);
			this->pictureBox22->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox22->TabIndex = 15;
			this->pictureBox22->TabStop = false;
			this->pictureBox22->Visible = false;
			// 
			// pictureBox23
			// 
			this->pictureBox23->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox23.Image")));
			this->pictureBox23->Location = System::Drawing::Point(181, 153);
			this->pictureBox23->Name = L"pictureBox23";
			this->pictureBox23->Size = System::Drawing::Size(5, 70);
			this->pictureBox23->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox23->TabIndex = 15;
			this->pictureBox23->TabStop = false;
			this->pictureBox23->Visible = false;
			// 
			// qFormMatr00
			// 
			this->qFormMatr00->BackColor = System::Drawing::Color::White;
			this->qFormMatr00->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->qFormMatr00->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->qFormMatr00->Location = System::Drawing::Point(71, 153);
			this->qFormMatr00->Name = L"qFormMatr00";
			this->qFormMatr00->ReadOnly = true;
			this->qFormMatr00->Size = System::Drawing::Size(53, 19);
			this->qFormMatr00->TabIndex = 13;
			this->qFormMatr00->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->qFormMatr00->Visible = false;
			// 
			// qFormMatr10
			// 
			this->qFormMatr10->BackColor = System::Drawing::Color::White;
			this->qFormMatr10->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->qFormMatr10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->qFormMatr10->Location = System::Drawing::Point(71, 197);
			this->qFormMatr10->Name = L"qFormMatr10";
			this->qFormMatr10->ReadOnly = true;
			this->qFormMatr10->Size = System::Drawing::Size(53, 19);
			this->qFormMatr10->TabIndex = 13;
			this->qFormMatr10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->qFormMatr10->Visible = false;
			// 
			// qFormMatr01
			// 
			this->qFormMatr01->BackColor = System::Drawing::Color::White;
			this->qFormMatr01->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->qFormMatr01->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->qFormMatr01->Location = System::Drawing::Point(128, 153);
			this->qFormMatr01->Name = L"qFormMatr01";
			this->qFormMatr01->ReadOnly = true;
			this->qFormMatr01->Size = System::Drawing::Size(53, 19);
			this->qFormMatr01->TabIndex = 13;
			this->qFormMatr01->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->qFormMatr01->Visible = false;
			// 
			// qFormMatr11
			// 
			this->qFormMatr11->BackColor = System::Drawing::Color::White;
			this->qFormMatr11->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->qFormMatr11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->qFormMatr11->Location = System::Drawing::Point(128, 197);
			this->qFormMatr11->Name = L"qFormMatr11";
			this->qFormMatr11->ReadOnly = true;
			this->qFormMatr11->Size = System::Drawing::Size(53, 19);
			this->qFormMatr11->TabIndex = 13;
			this->qFormMatr11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->qFormMatr11->Visible = false;
			// 
			// A_equal_label
			// 
			this->A_equal_label->AutoSize = true;
			this->A_equal_label->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->A_equal_label->Location = System::Drawing::Point(26, 175);
			this->A_equal_label->Name = L"A_equal_label";
			this->A_equal_label->Size = System::Drawing::Size(39, 24);
			this->A_equal_label->TabIndex = 11;
			this->A_equal_label->Text = L"A =";
			this->A_equal_label->Visible = false;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label4->Location = System::Drawing::Point(26, 295);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(76, 24);
			this->label4->TabIndex = 11;
			this->label4->Text = L"|A-λE| =";
			this->label4->Visible = false;
			// 
			// charact_eq_label
			// 
			this->charact_eq_label->AutoSize = true;
			this->charact_eq_label->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->charact_eq_label->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(42)), static_cast<System::Int32>(static_cast<System::Byte>(87)),
				static_cast<System::Int32>(static_cast<System::Byte>(154)));
			this->charact_eq_label->Location = System::Drawing::Point(3, 240);
			this->charact_eq_label->Name = L"charact_eq_label";
			this->charact_eq_label->Size = System::Drawing::Size(318, 22);
			this->charact_eq_label->TabIndex = 18;
			this->charact_eq_label->Text = L"Характеристическое уравнение";
			this->charact_eq_label->Visible = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->Location = System::Drawing::Point(110, 273);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(2, 70);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox2->TabIndex = 15;
			this->pictureBox2->TabStop = false;
			this->pictureBox2->Visible = false;
			// 
			// pictureBox4
			// 
			this->pictureBox4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox4.Image")));
			this->pictureBox4->Location = System::Drawing::Point(226, 273);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(2, 70);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox4->TabIndex = 15;
			this->pictureBox4->TabStop = false;
			this->pictureBox4->Visible = false;
			// 
			// textBox7
			// 
			this->textBox7->BackColor = System::Drawing::Color::White;
			this->textBox7->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox7->Location = System::Drawing::Point(114, 273);
			this->textBox7->Name = L"textBox7";
			this->textBox7->ReadOnly = true;
			this->textBox7->Size = System::Drawing::Size(53, 19);
			this->textBox7->TabIndex = 13;
			this->textBox7->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox7->Visible = false;
			// 
			// textBox8
			// 
			this->textBox8->BackColor = System::Drawing::Color::White;
			this->textBox8->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox8->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->textBox8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox8->Location = System::Drawing::Point(114, 317);
			this->textBox8->Name = L"textBox8";
			this->textBox8->ReadOnly = true;
			this->textBox8->Size = System::Drawing::Size(53, 19);
			this->textBox8->TabIndex = 13;
			this->textBox8->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox8->Visible = false;
			// 
			// textBox9
			// 
			this->textBox9->BackColor = System::Drawing::Color::White;
			this->textBox9->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox9->Cursor = System::Windows::Forms::Cursors::Default;
			this->textBox9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox9->Location = System::Drawing::Point(171, 273);
			this->textBox9->Name = L"textBox9";
			this->textBox9->ReadOnly = true;
			this->textBox9->Size = System::Drawing::Size(53, 19);
			this->textBox9->TabIndex = 13;
			this->textBox9->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox9->Visible = false;
			// 
			// textBox10
			// 
			this->textBox10->BackColor = System::Drawing::Color::White;
			this->textBox10->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox10->Location = System::Drawing::Point(171, 317);
			this->textBox10->Name = L"textBox10";
			this->textBox10->ReadOnly = true;
			this->textBox10->Size = System::Drawing::Size(53, 19);
			this->textBox10->TabIndex = 13;
			this->textBox10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox10->Visible = false;
			// 
			// lambda1
			// 
			this->lambda1->AutoSize = true;
			this->lambda1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->lambda1->Location = System::Drawing::Point(15, 468);
			this->lambda1->Name = L"lambda1";
			this->lambda1->Size = System::Drawing::Size(83, 20);
			this->lambda1->TabIndex = 19;
			this->lambda1->Text = L"1)  λ = λ₁ =";
			this->lambda1->Visible = false;
			// 
			// lambda
			// 
			this->lambda->AutoSize = true;
			this->lambda->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->lambda->Location = System::Drawing::Point(15, 468);
			this->lambda->Name = L"lambda";
			this->lambda->Size = System::Drawing::Size(35, 20);
			this->lambda->TabIndex = 19;
			this->lambda->Text = L" λ =";
			this->lambda->Visible = false;
			// 
			// lambda2
			// 
			this->lambda2->AutoSize = true;
			this->lambda2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->lambda2->Location = System::Drawing::Point(15, 684);
			this->lambda2->Name = L"lambda2";
			this->lambda2->Size = System::Drawing::Size(83, 20);
			this->lambda2->TabIndex = 19;
			this->lambda2->Text = L"2)  λ = λ₂ =";
			this->lambda2->Visible = false;
			// 
			// ownVal
			// 
			this->ownVal->BackColor = System::Drawing::Color::White;
			this->ownVal->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownVal->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownVal->Location = System::Drawing::Point(53, 467);
			this->ownVal->Name = L"ownVal";
			this->ownVal->ReadOnly = true;
			this->ownVal->Size = System::Drawing::Size(44, 19);
			this->ownVal->TabIndex = 10;
			this->ownVal->Visible = false;
			// 
			// pictureBox6
			// 
			this->pictureBox6->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox6.Image")));
			this->pictureBox6->Location = System::Drawing::Point(312, 592);
			this->pictureBox6->Name = L"pictureBox6";
			this->pictureBox6->Size = System::Drawing::Size(5, 70);
			this->pictureBox6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox6->TabIndex = 15;
			this->pictureBox6->TabStop = false;
			this->pictureBox6->Visible = false;
			// 
			// textBox11
			// 
			this->textBox11->BackColor = System::Drawing::Color::White;
			this->textBox11->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox11->Location = System::Drawing::Point(259, 592);
			this->textBox11->Name = L"textBox11";
			this->textBox11->ReadOnly = true;
			this->textBox11->Size = System::Drawing::Size(53, 19);
			this->textBox11->TabIndex = 13;
			this->textBox11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox11->Visible = false;
			// 
			// textBox12
			// 
			this->textBox12->BackColor = System::Drawing::Color::White;
			this->textBox12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->textBox12->Location = System::Drawing::Point(259, 636);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(53, 26);
			this->textBox12->TabIndex = 13;
			this->textBox12->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBox12->Visible = false;
			this->textBox12->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::textBox12_KeyUp);
			// 
			// e2_1_label
			// 
			this->e2_1_label->AutoSize = true;
			this->e2_1_label->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->e2_1_label->Location = System::Drawing::Point(209, 614);
			this->e2_1_label->Name = L"e2_1_label";
			this->e2_1_label->Size = System::Drawing::Size(41, 24);
			this->e2_1_label->TabIndex = 17;
			this->e2_1_label->Text = L"ā₂ =";
			this->e2_1_label->Visible = false;
			// 
			// pictureBox7
			// 
			this->pictureBox7->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox7.Image")));
			this->pictureBox7->Location = System::Drawing::Point(254, 592);
			this->pictureBox7->Name = L"pictureBox7";
			this->pictureBox7->Size = System::Drawing::Size(5, 70);
			this->pictureBox7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox7->TabIndex = 15;
			this->pictureBox7->TabStop = false;
			this->pictureBox7->Visible = false;
			// 
			// charactEq
			// 
			this->charactEq->BackColor = System::Drawing::Color::White;
			this->charactEq->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->charactEq->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->charactEq->Location = System::Drawing::Point(260, 294);
			this->charactEq->Name = L"charactEq";
			this->charactEq->ReadOnly = true;
			this->charactEq->Size = System::Drawing::Size(161, 22);
			this->charactEq->TabIndex = 20;
			this->charactEq->Visible = false;
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label17->Location = System::Drawing::Point(232, 295);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(21, 24);
			this->label17->TabIndex = 21;
			this->label17->Text = L"=";
			this->label17->Visible = false;
			// 
			// own_vect_label
			// 
			this->own_vect_label->AutoSize = true;
			this->own_vect_label->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->own_vect_label->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(42)), static_cast<System::Int32>(static_cast<System::Byte>(87)),
				static_cast<System::Int32>(static_cast<System::Byte>(154)));
			this->own_vect_label->Location = System::Drawing::Point(3, 442);
			this->own_vect_label->Name = L"own_vect_label";
			this->own_vect_label->Size = System::Drawing::Size(219, 22);
			this->own_vect_label->TabIndex = 9;
			this->own_vect_label->Text = L"Собственные вектора";
			this->own_vect_label->Visible = false;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(20, 381);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(35, 20);
			this->label2->TabIndex = 19;
			this->label2->Text = L"λ₁ =";
			this->label2->Visible = false;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label5->Location = System::Drawing::Point(20, 413);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(35, 20);
			this->label5->TabIndex = 19;
			this->label5->Text = L"λ₂ =";
			this->label5->Visible = false;
			// 
			// ownValue2
			// 
			this->ownValue2->BackColor = System::Drawing::Color::White;
			this->ownValue2->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownValue2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownValue2->Location = System::Drawing::Point(59, 412);
			this->ownValue2->Name = L"ownValue2";
			this->ownValue2->ReadOnly = true;
			this->ownValue2->Size = System::Drawing::Size(44, 19);
			this->ownValue2->TabIndex = 10;
			this->ownValue2->Visible = false;
			// 
			// ownValue1
			// 
			this->ownValue1->BackColor = System::Drawing::Color::White;
			this->ownValue1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->ownValue1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->ownValue1->Location = System::Drawing::Point(59, 380);
			this->ownValue1->Name = L"ownValue1";
			this->ownValue1->ReadOnly = true;
			this->ownValue1->Size = System::Drawing::Size(44, 19);
			this->ownValue1->TabIndex = 10;
			this->ownValue1->Visible = false;
			// 
			// chart1
			// 
			chartArea1->AxisX->LabelStyle->TruncatedLabels = true;
			chartArea1->AxisY->LabelStyle->TruncatedLabels = true;
			chartArea1->CursorX->IsUserEnabled = true;
			chartArea1->CursorX->LineColor = System::Drawing::Color::Transparent;
			chartArea1->CursorX->SelectionColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(255)));
			chartArea1->CursorY->IsUserEnabled = true;
			chartArea1->CursorY->LineColor = System::Drawing::Color::Transparent;
			chartArea1->CursorY->SelectionColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(255)));
			chartArea1->Name = L"ChartArea1";
			this->chart1->ChartAreas->Add(chartArea1);
			legend1->Enabled = false;
			legend1->Name = L"Legend1";
			this->chart1->Legends->Add(legend1);
			this->chart1->Location = System::Drawing::Point(0, 1084);
			this->chart1->Name = L"chart1";
			series1->ChartArea = L"ChartArea1";
			series1->Legend = L"Legend1";
			series1->Name = L"Series1";
			this->chart1->Series->Add(series1);
			this->chart1->Size = System::Drawing::Size(473, 473);
			this->chart1->TabIndex = 24;
			this->chart1->Text = L"chart1";
			this->chart1->Visible = false;
			this->chart1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainWnd::chart1_MouseDown);
			this->chart1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainWnd::chart1_MouseMove);
			this->chart1->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &MainWnd::chart1_MouseWheel);
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label9->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(42)), static_cast<System::Int32>(static_cast<System::Byte>(87)),
				static_cast<System::Int32>(static_cast<System::Byte>(154)));
			this->label9->Location = System::Drawing::Point(0, 1059);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(153, 22);
			this->label9->TabIndex = 9;
			this->label9->Text = L"График кривой";
			this->label9->Visible = false;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label10->ForeColor = System::Drawing::Color::Black;
			this->label10->Location = System::Drawing::Point(47, 116);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(131, 22);
			this->label10->TabIndex = 9;
			this->label10->Text = L"вид кривой:";
			// 
			// curveType
			// 
			this->curveType->BackColor = System::Drawing::Color::White;
			this->curveType->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->curveType->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->curveType->Location = System::Drawing::Point(178, 113);
			this->curveType->Name = L"curveType";
			this->curveType->Size = System::Drawing::Size(100, 24);
			this->curveType->TabIndex = 25;
			// 
			// example
			// 
			this->example->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->example->Location = System::Drawing::Point(377, 90);
			this->example->Name = L"example";
			this->example->Size = System::Drawing::Size(90, 30);
			this->example->TabIndex = 8;
			this->example->Text = L"Пример";
			this->example->UseVisualStyleBackColor = true;
			this->example->Click += gcnew System::EventHandler(this, &MainWnd::example_Click);
			// 
			// canonical_equation
			// 
			this->canonical_equation->AutoScroll = true;
			this->canonical_equation->Controls->Add(this->sign);
			this->canonical_equation->Controls->Add(this->curveType);
			this->canonical_equation->Controls->Add(this->right_part);
			this->canonical_equation->Controls->Add(this->b2);
			this->canonical_equation->Controls->Add(this->a2);
			this->canonical_equation->Controls->Add(this->bar2);
			this->canonical_equation->Controls->Add(this->bar1);
			this->canonical_equation->Controls->Add(this->pictureBox14);
			this->canonical_equation->Controls->Add(this->pictureBox12);
			this->canonical_equation->Controls->Add(this->label6);
			this->canonical_equation->Controls->Add(this->label10);
			this->canonical_equation->Location = System::Drawing::Point(0, 894);
			this->canonical_equation->Name = L"canonical_equation";
			this->canonical_equation->Size = System::Drawing::Size(454, 160);
			this->canonical_equation->TabIndex = 26;
			this->canonical_equation->Visible = false;
			// 
			// sign
			// 
			this->sign->AutoSize = true;
			this->sign->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->sign->Location = System::Drawing::Point(93, 50);
			this->sign->Name = L"sign";
			this->sign->Size = System::Drawing::Size(27, 29);
			this->sign->TabIndex = 11;
			this->sign->Text = L"+";
			// 
			// right_part
			// 
			this->right_part->AutoSize = true;
			this->right_part->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->right_part->Location = System::Drawing::Point(173, 50);
			this->right_part->Name = L"right_part";
			this->right_part->Size = System::Drawing::Size(33, 29);
			this->right_part->TabIndex = 11;
			this->right_part->Text = L"= ";
			// 
			// b2
			// 
			this->b2->AutoSize = true;
			this->b2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->b2->Location = System::Drawing::Point(127, 69);
			this->b2->Name = L"b2";
			this->b2->Size = System::Drawing::Size(0, 29);
			this->b2->TabIndex = 11;
			// 
			// a2
			// 
			this->a2->AutoSize = true;
			this->a2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->a2->Location = System::Drawing::Point(42, 69);
			this->a2->Name = L"a2";
			this->a2->Size = System::Drawing::Size(0, 29);
			this->a2->TabIndex = 11;
			// 
			// bar2
			// 
			this->bar2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"bar2.Image")));
			this->bar2->Location = System::Drawing::Point(135, 64);
			this->bar2->Name = L"bar2";
			this->bar2->Size = System::Drawing::Size(31, 2);
			this->bar2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->bar2->TabIndex = 10;
			this->bar2->TabStop = false;
			// 
			// bar1
			// 
			this->bar1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"bar1.Image")));
			this->bar1->Location = System::Drawing::Point(48, 64);
			this->bar1->Name = L"bar1";
			this->bar1->Size = System::Drawing::Size(31, 2);
			this->bar1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->bar1->TabIndex = 10;
			this->bar1->TabStop = false;
			// 
			// pictureBox14
			// 
			this->pictureBox14->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox14.Image")));
			this->pictureBox14->Location = System::Drawing::Point(139, 30);
			this->pictureBox14->Name = L"pictureBox14";
			this->pictureBox14->Size = System::Drawing::Size(24, 31);
			this->pictureBox14->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox14->TabIndex = 10;
			this->pictureBox14->TabStop = false;
			// 
			// pictureBox12
			// 
			this->pictureBox12->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox12.Image")));
			this->pictureBox12->Location = System::Drawing::Point(52, 35);
			this->pictureBox12->Name = L"pictureBox12";
			this->pictureBox12->Size = System::Drawing::Size(25, 26);
			this->pictureBox12->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox12->TabIndex = 10;
			this->pictureBox12->TabStop = false;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Courier New", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label6->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(42)), static_cast<System::Int32>(static_cast<System::Byte>(87)),
				static_cast<System::Int32>(static_cast<System::Byte>(154)));
			this->label6->Location = System::Drawing::Point(0, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(252, 22);
			this->label6->TabIndex = 9;
			this->label6->Text = L"Каноническое уравнение";
			// 
			// MainWnd
			// 
			this->AcceptButton = this->solve;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoScroll = true;
			this->BackColor = System::Drawing::Color::White;
			this->ClientSize = System::Drawing::Size(691, 733);
			this->Controls->Add(this->canonical_equation);
			this->Controls->Add(this->chart1);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->charactEq);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->lambda);
			this->Controls->Add(this->lambda2);
			this->Controls->Add(this->lambda1);
			this->Controls->Add(this->charact_eq_label);
			this->Controls->Add(this->quadriqMatrix_label);
			this->Controls->Add(this->pictureBox7);
			this->Controls->Add(this->pictureBox21);
			this->Controls->Add(this->pictureBox10);
			this->Controls->Add(this->e2_1_label);
			this->Controls->Add(this->e2_2_label);
			this->Controls->Add(this->e1_label);
			this->Controls->Add(this->right_arrow2_label);
			this->Controls->Add(this->right_arrow1_label);
			this->Controls->Add(this->stepMatr2_11);
			this->Controls->Add(this->stepMatr1_11);
			this->Controls->Add(this->stepMatr2_01);
			this->Controls->Add(this->stepMatr1_01);
			this->Controls->Add(this->temp2_11);
			this->Controls->Add(this->textBox10);
			this->Controls->Add(this->qFormMatr11);
			this->Controls->Add(this->temp1_11);
			this->Controls->Add(this->stepMatr2_10);
			this->Controls->Add(this->temp2_01);
			this->Controls->Add(this->stepMatr1_10);
			this->Controls->Add(this->textBox9);
			this->Controls->Add(this->qFormMatr01);
			this->Controls->Add(this->temp1_01);
			this->Controls->Add(this->textBox12);
			this->Controls->Add(this->ownVect2_2);
			this->Controls->Add(this->textBox11);
			this->Controls->Add(this->ownVect2_1);
			this->Controls->Add(this->ownVect1_2);
			this->Controls->Add(this->temp2_10);
			this->Controls->Add(this->ownVect1_1);
			this->Controls->Add(this->stepMatr2_00);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->qFormMatr10);
			this->Controls->Add(this->temp1_10);
			this->Controls->Add(this->stepMatr1_00);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->temp2_00);
			this->Controls->Add(this->qFormMatr00);
			this->Controls->Add(this->temp1_00);
			this->Controls->Add(this->ownVal);
			this->Controls->Add(this->ownValue1);
			this->Controls->Add(this->ownValue2);
			this->Controls->Add(this->ownVal1);
			this->Controls->Add(this->ownVal2);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->own_vect_label);
			this->Controls->Add(this->own_val_label);
			this->Controls->Add(this->example);
			this->Controls->Add(this->solve);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->pictureBox20);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pictureBox6);
			this->Controls->Add(this->pictureBox9);
			this->Controls->Add(this->pictureBox19);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->pictureBox11);
			this->Controls->Add(this->pictureBox17);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->pictureBox16);
			this->Controls->Add(this->pictureBox4);
			this->Controls->Add(this->pictureBox8);
			this->Controls->Add(this->pictureBox23);
			this->Controls->Add(this->pictureBox5);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox13);
			this->Controls->Add(this->pictureBox22);
			this->Controls->Add(this->pictureBox3);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->A_equal_label);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MainWnd";
			this->Text = L"Calculator";
			this->Load += gcnew System::EventHandler(this, &MainWnd::MainWnd_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainWnd::MainWnd_KeyDown);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox10))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox11))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox13))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox16))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox17))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox19))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox20))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox21))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox22))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox23))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->EndInit();
			this->canonical_equation->ResumeLayout(false);
			this->canonical_equation->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bar2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bar1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox14))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox12))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {		
		AboutDlg^ about = gcnew AboutDlg;
		about->ShowDialog(this);
	}

private: System::Void ShowBegin() {
	quadriqMatrix_label->Visible = true;
	A_equal_label->Visible = true;
	charact_eq_label->Visible = true;
	label4->Visible = true;
	label17->Visible = true;

	pictureBox22->Visible = true;
	pictureBox23->Visible = true;
	pictureBox2->Visible = true;
	pictureBox4->Visible = true;

	qFormMatr00->Visible = true;
	qFormMatr01->Visible = true;
	qFormMatr10->Visible = true;
	qFormMatr11->Visible = true;
	charactEq->Visible = true;
	charactEq->Text = L"";

	textBox7->Visible = true;
	textBox8->Visible = true;
	textBox9->Visible = true;
	textBox10->Visible = true;

	own_val_label->Visible = true;
	label2->Visible = true;
	label5->Visible = true;
	ownValue1->Visible = true;
	ownValue2->Visible = true;
}

private: System::Void HideBegin() {
	quadriqMatrix_label->Visible = false;
	A_equal_label->Visible = false;
	charact_eq_label->Visible = false;
	label4->Visible = false;
	label17->Visible = false;

	pictureBox22->Visible = false;
	pictureBox23->Visible = false;
	pictureBox2->Visible = false;
	pictureBox4->Visible = false;

	qFormMatr00->Visible = false;
	qFormMatr01->Visible = false;
	qFormMatr10->Visible = false;
	qFormMatr11->Visible = false;
	charactEq->Visible = false;
	charactEq->Text = L"";

	textBox7->Visible = false;
	textBox8->Visible = false;
	textBox9->Visible = false;
	textBox10->Visible = false;

	own_val_label->Visible = false;
	label2->Visible = false;
	label5->Visible = false;
	ownValue1->Visible = false;
	ownValue2->Visible = false;
}

private: System::Void Show1ov() {
	own_vect_label->Visible = true;

	lambda->Visible = true;
	ownVal->Visible = true;

	label3->Visible = true;
	pictureBox3->Visible = true;
	temp1_00->Visible = true;
	temp1_01->Visible = true;
	temp1_10->Visible = true;
	temp1_11->Visible = true;
	pictureBox5->Visible = true;
	right_arrow1_label->Visible = true;	
	pictureBox8->Visible = true;
	stepMatr1_00->Visible = true;
	stepMatr1_01->Visible = true;
	stepMatr1_10->Visible = true;
	stepMatr1_11->Visible = true;
	pictureBox9->Visible = true;
		
	e1_label->Visible = true;
	pictureBox10->Visible = true;
	ownVect1_1->Visible = true;
	ownVect1_2->Visible = true;
	ownVect1_2->ReadOnly = false; 
	ownVect1_2->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
	pictureBox11->Visible = true;
	e2_1_label->Visible = true;
	pictureBox7->Visible = true;
	textBox11->Visible = true;
	textBox12->Visible = true;
	textBox12->ReadOnly = false;
	textBox12->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
	pictureBox6->Visible = true;

	canonical_equation->Visible = true;
	canonical_equation->Location = System::Drawing::Point(0, 684);

	label9->Visible = true;
	label9->Location = System::Drawing::Point(0, 845);
	chart1->Visible = true;
	chart1->Location = System::Drawing::Point(0, 877);
}

private: System::Void Show2ov() {
	own_vect_label->Visible = true;
	lambda1->Visible = true;
	ownVal1->Visible = true;
	label3->Visible = true;
	pictureBox3->Visible = true;
	temp1_00->Visible = true;
	temp1_01->Visible = true;
	temp1_10->Visible = true;
	temp1_11->Visible = true;
	pictureBox5->Visible = true;
	right_arrow1_label->Visible = true;
	pictureBox8->Visible = true;
	stepMatr1_00->Visible = true;
	stepMatr1_01->Visible = true;
	stepMatr1_10->Visible = true;
	stepMatr1_11->Visible = true;
	pictureBox9->Visible = true;

	e1_label->Visible = true;
	pictureBox10->Visible = true;
	ownVect1_1->Visible = true;
	ownVect1_2->Visible = true;
	ownVect1_2->ReadOnly = false;
	ownVect1_2->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
	pictureBox11->Visible = true;

	lambda2->Visible = true;
	ownVal2->Visible = true;

	label7->Visible = true;
	pictureBox13->Visible = true;
	temp2_00->Visible = true;
	temp2_01->Visible = true;
	temp2_10->Visible = true;
	temp2_11->Visible = true;
	pictureBox16->Visible = true;
	right_arrow2_label->Visible = true;
	pictureBox17->Visible = true;
	stepMatr2_00->Visible = true;
	stepMatr2_01->Visible = true;
	stepMatr2_10->Visible = true;
	stepMatr2_11->Visible = true;
	pictureBox20->Visible = true;

	e2_2_label->Visible = true;
	pictureBox21->Visible = true;
	ownVect2_1->Visible = true;
	ownVect2_2->Visible = true;
	ownVect2_2->ReadOnly = false;
	ownVect2_2->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
	pictureBox19->Visible = true;

	canonical_equation->Visible = true;
	canonical_equation->Location = System::Drawing::Point(0, 902);

	label9->Visible = true;
	label9->Location = System::Drawing::Point(0, 1059);
	chart1->Visible = true;
	chart1->Location = System::Drawing::Point(0, 1092);
}

private: System::Void HideAllOV() {
	own_vect_label->Visible = false;
	lambda->Visible = false;
	ownVal->Visible = false;
	e2_1_label->Visible = false;
	pictureBox7->Visible = false;
	textBox11->Visible = false;
	textBox12->Visible = false;
	pictureBox6->Visible = false;
	own_val_label->Visible = false;
	lambda1->Visible = false;
	ownVal1->Visible = false;
	label3->Visible = false;
	pictureBox3->Visible = false;
	temp1_00->Visible = false;
	temp1_01->Visible = false;
	temp1_10->Visible = false;
	temp1_11->Visible = false;
	pictureBox5->Visible = false;
	right_arrow1_label->Visible = false;
	pictureBox8->Visible = false;
	stepMatr1_00->Visible = false;
	stepMatr1_01->Visible = false;
	stepMatr1_10->Visible = false;
	stepMatr1_11->Visible = false;
	pictureBox9->Visible = false;
	e1_label->Visible = false;
	pictureBox10->Visible = false;
	ownVect1_1->Visible = false;
	ownVect1_2->Visible = false;
	pictureBox11->Visible = false;
	lambda2->Visible = false;
	ownVal2->Visible = false;
	label7->Visible = false;
	pictureBox13->Visible = false;
	temp2_00->Visible = false;
	temp2_01->Visible = false;
	temp2_10->Visible = false;
	temp2_11->Visible = false;
	pictureBox16->Visible = false;
	right_arrow2_label->Visible = false;
	pictureBox17->Visible = false;
	stepMatr2_00->Visible = false;
	stepMatr2_01->Visible = false;
	stepMatr2_10->Visible = false;
	stepMatr2_11->Visible = false;
	pictureBox20->Visible = false;
	e2_2_label->Visible = false;
	pictureBox21->Visible = false;
	ownVect2_1->Visible = false;
	ownVect2_2->Visible = false;
	pictureBox19->Visible = false;

	canonical_equation->Visible = false;
	pictureBox12->Visible = false;
	pictureBox14->Visible = false;
	bar1->Visible = false;
	bar2->Visible = false;
	a2->Visible = false;
	b2->Visible = false;
	sign->Visible = false;
	right_part->Text = L"= ";
	sign->Text = L"+";

	label9->Visible = false;
	chart1->Visible = false;
}

private: System::Void solve_Click(System::Object^  sender, System::EventArgs^  e) {
	try {
		try {

			Binomial<double> equation;
			double x2 = System::Double::Parse(textBox1->Text),
				y2 = System::Double::Parse(textBox2->Text),
				xy = System::Double::Parse(textBox3->Text),
				x = System::Double::Parse(textBox4->Text),
				y = System::Double::Parse(textBox5->Text),
				c = System::Double::Parse(textBox6->Text);

			SquareMatrix <double> matrix;

			matrix[0][0] = x2;
			matrix[0][1] = xy/2;
			matrix[1][0] = xy/2;
			matrix[1][1] = y2;

			QubicMatrix <double> deltaMatrix;

			deltaMatrix[0][0] = x2;
			deltaMatrix[1][1] = y2;
			deltaMatrix[2][2] = c;
			deltaMatrix[0][1] = deltaMatrix[1][0] = xy / 2;
			deltaMatrix[0][2] = deltaMatrix[2][0] = x / 2;
			deltaMatrix[1][2] = deltaMatrix[2][1] = y / 2;

			SquareMatrix <double> temp;

			temp[0][0] = x2;
			temp[0][1] = x/2;
			temp[1][0] = x/2;
			temp[1][1] = c;

			double B = temp.determinant();

			temp[0][0] = y2;
			temp[0][1] = y / 2;
			temp[1][0] = y / 2;
			temp[1][1] = c;

			B += temp.determinant();
			double delta = deltaMatrix.determinant();
			double D = matrix.determinant();
			double I = x2 + y2;
			 
			HideAllOV();
			this->ClientSize = System::Drawing::Size(SOLVE_WIDTH, SOLVE_HEIGHT);
			this->Location = Point((Screen::PrimaryScreen->Bounds.Width - SOLVE_WIDTH) / 2,
				(Screen::PrimaryScreen->Bounds.Height - SOLVE_HEIGHT) / 2);

			ShowBegin();
			qFormMatr00->Text = Math::Round(matrix[0][0], PRECISION).ToString();
			qFormMatr10->Text = Math::Round(matrix[1][0], PRECISION).ToString();
			qFormMatr01->Text = Math::Round(matrix[0][1], PRECISION).ToString();
			qFormMatr11->Text = Math::Round(matrix[1][1], PRECISION).ToString();

			textBox7->Text = Math::Round(matrix[0][0], PRECISION).ToString();
			textBox7->Text += L"-λ";
			textBox8->Text = Math::Round(matrix[1][0], PRECISION).ToString();
			textBox9->Text = Math::Round(matrix[0][1], PRECISION).ToString();
			textBox10->Text = Math::Round(matrix[1][1], PRECISION).ToString();
			textBox10->Text += L"-λ";

			Binomial<double> binom;
			binom = matrix.сharacteristicEquation();
			if (binom[2] != 0)
			{
				if (binom[2] != 1) charactEq->Text += binom[2].ToString();
				charactEq->Text += L"λ²";
			}
			if (binom[1] < 0)
			{
				charactEq->Text += L" - ";
				if (binom[1] != 1) charactEq->Text += (-binom[1]).ToString();
				charactEq->Text += L"λ";
			}
			if (binom[1] > 0)
			{
				charactEq->Text += L" + ";
				charactEq->Text += binom[1].ToString();
				charactEq->Text += L"λ";
			}
			if (binom[0] < 0)
			{
				charactEq->Text += L" - ";
				if (binom[0] != 1) charactEq->Text += (-binom[0]).ToString();
			}
			if (binom[0] > 0)
			{
				charactEq->Text += L" + ";
				charactEq->Text += binom[0].ToString();
			}
			charactEq->Text += L" = 0";
			charactEq->Size = charactEq->GetPreferredSize(Size.Empty);
			double ov[2];
			matrix.ownValues(ov);
			SquareMatrix<double> stepMatrix, ovect;
			ownValue1->Text = Math::Round(ov[0], PRECISION).ToString();
			ownValue1->Size = ownValue1->GetPreferredSize(Size.Empty);
			ownValue2->Text = Math::Round(ov[1], PRECISION).ToString();
			ownValue2->Size = ownValue2->GetPreferredSize(Size.Empty);
			if (ov[0] == ov[1])
			{
				Show1ov();
				ownVal->Text = Math::Round(ov[0], PRECISION).ToString();
				ownVal->Size = ownVal->GetPreferredSize(Size.Empty);

				temp1_00->Text = Math::Round((matrix[0][0] - ov[0]), PRECISION).ToString();
				temp1_01->Text = Math::Round(matrix[0][1], PRECISION).ToString();
				temp1_10->Text = Math::Round(matrix[1][0], PRECISION).ToString();
				temp1_11->Text = Math::Round((matrix[1][1] - ov[0]), PRECISION).ToString();

				stepMatrix = matrix;
				stepMatrix[0][0] -= ov[0];
				stepMatrix[1][1] -= ov[0];
				stepMatrix = stepMatrix.getStepForm();
				stepMatr1_00->Text = Math::Round(stepMatrix[0][0], PRECISION).ToString();
				stepMatr1_01->Text = Math::Round(stepMatrix[0][1], PRECISION).ToString();
				stepMatr1_10->Text = Math::Round(stepMatrix[1][0], PRECISION).ToString();
				stepMatr1_11->Text = Math::Round(stepMatrix[1][1], PRECISION).ToString();

				ovect = matrix.getOwnVectors();
				OV1X = ovect[0][0];
				OV1Y = ovect[1][0];

				OV2X = ovect[0][1];
				OV2Y = ovect[1][1];

				ownVect1_1->Text = Math::Round(ovect[0][0], PRECISION).ToString();
				ownVect1_2->Text = Math::Round(ovect[1][0], PRECISION).ToString();
				if (OV1Y == 0) {
					ownVect1_2->ReadOnly = true;
					ownVect1_2->BorderStyle = System::Windows::Forms::BorderStyle::None;
				}
				textBox11->Text = Math::Round(ovect[0][1], PRECISION).ToString();
				textBox12->Text = Math::Round(ovect[1][1], PRECISION).ToString();
				if (OV2Y == 0) {
					textBox12->ReadOnly = true;
					textBox12->BorderStyle = System::Windows::Forms::BorderStyle::None;
				}
			}
			else
			{
				Show2ov();
				ownVal1->Text = Math::Round(ov[0], PRECISION).ToString();
				ownVal1->Size = ownVal1->GetPreferredSize(Size.Empty);

				temp1_00->Text = Math::Round((matrix[0][0] - ov[0]), PRECISION).ToString();
				temp1_01->Text = Math::Round(matrix[0][1], PRECISION).ToString();
				temp1_10->Text = Math::Round(matrix[1][0], PRECISION).ToString();
				temp1_11->Text = Math::Round((matrix[1][1] - ov[0]), PRECISION).ToString();

				stepMatrix = matrix;
				stepMatrix[0][0] -= ov[0];
				stepMatrix[1][1] -= ov[0];
				stepMatrix = stepMatrix.getStepForm();
				stepMatr1_00->Text = Math::Round(stepMatrix[0][0], PRECISION).ToString();
				stepMatr1_01->Text = Math::Round(stepMatrix[0][1], PRECISION).ToString();
				stepMatr1_10->Text = Math::Round(stepMatrix[1][0], PRECISION).ToString();
				stepMatr1_11->Text = Math::Round(stepMatrix[1][1], PRECISION).ToString();

				ovect = matrix.getOwnVectors();
				OV1X = ovect[0][0];
				OV1Y = ovect[1][0];

				OV2X = ovect[0][1];
				OV2Y = ovect[1][1];
				ownVect1_1->Text = Math::Round(ovect[0][0], PRECISION).ToString();
				ownVect1_2->Text = Math::Round(ovect[1][0], PRECISION).ToString();
				if (OV1Y == 0) {
					ownVect1_2->ReadOnly = true;
					ownVect1_2->BorderStyle = System::Windows::Forms::BorderStyle::None;
				}
				ownVect2_1->Text = Math::Round(ovect[0][1], PRECISION).ToString();
				ownVect2_2->Text = Math::Round(ovect[1][1], PRECISION).ToString();
				if (OV2Y == 0) {
					ownVect2_2->ReadOnly = true;
					ownVect2_2->BorderStyle = System::Windows::Forms::BorderStyle::None;
				}

				ownVal2->Text = Math::Round(ov[1], PRECISION).ToString();
				ownVal2->Size = ownVal2->GetPreferredSize(Size.Empty);

				temp2_00->Text = Math::Round((matrix[0][0] - ov[1]), PRECISION).ToString();
				temp2_01->Text = Math::Round(matrix[0][1], PRECISION).ToString();
				temp2_10->Text = Math::Round(matrix[1][0], PRECISION).ToString();
				temp2_11->Text = Math::Round((matrix[1][1] - ov[1]), PRECISION).ToString();

				stepMatrix = matrix;
				stepMatrix[0][0] -= ov[1];
				stepMatrix[1][1] -= ov[1];
				stepMatrix = stepMatrix.getStepForm();
				stepMatr2_00->Text = Math::Round(stepMatrix[0][0], PRECISION).ToString();
				stepMatr2_01->Text = Math::Round(stepMatrix[0][1], PRECISION).ToString();
				stepMatr2_10->Text = Math::Round(stepMatrix[1][0], PRECISION).ToString();
				stepMatr2_11->Text = Math::Round(stepMatrix[1][1], PRECISION).ToString();
			}		

			double** slauMatrix = new double*[2];
			for (int i = 0; i < 2; i++) {
				slauMatrix[i] = new double[3];
			}			
			
			slauMatrix[0][0] = x2;
			slauMatrix[1][1] = y2;
			slauMatrix[0][1] = slauMatrix[1][0] = xy / 2;
			slauMatrix[0][2] = -x / 2;
			slauMatrix[1][2] = -y / 2;			

			int AxisMax = 10;
			double interval = AxisMax / 5;
			if (delta != 0)
			{
				if (D > 0 && delta*I < 0)
				{
					if (I*I == 4 * D) curveType->Text = L"окружность";
					else curveType->Text = L"эллипс";

					pictureBox12->Visible = true;
					pictureBox12->Location = System::Drawing::Point(52, 35);
					pictureBox14->Visible = true;
					pictureBox14->Location = System::Drawing::Point(139, 30);
					bar1->Visible = true;
					bar1->Location = System::Drawing::Point(48, 64);
					bar2->Visible = true;
					bar2->Location = System::Drawing::Point(135, 64);
					a2->Visible = true;
					a2->Location = System::Drawing::Point(42, 69);
					b2->Visible = true;
					b2->Location = System::Drawing::Point(127, 69);
					sign->Visible = true;
					right_part->Text += L"1";
					right_part->Location = System::Drawing::Point(173, 50);

					a2->Text = Math::Round(Math::Sqrt(-delta / ov[0]) / ov[1], 1) + L"²";
					b2->Text = Math::Round(Math::Sqrt(-delta / ov[1]) / ov[0], 1) + L"²";

					AxisMax = int(Math::Sqrt(-delta / ov[0]) / ov[1] + 3);
					interval = AxisMax / 5.0;
				}
				if (D > 0 && delta*I > 0) {
					curveType->Text = L"мнимый эллипс";

					pictureBox12->Visible = true;
					pictureBox12->Location = System::Drawing::Point(52, 35);
					pictureBox14->Visible = true;
					pictureBox14->Location = System::Drawing::Point(139, 30);
					bar1->Visible = true;
					bar1->Location = System::Drawing::Point(48, 64);
					bar2->Visible = true;
					bar2->Location = System::Drawing::Point(135, 64);
					a2->Visible = true;
					a2->Location = System::Drawing::Point(42, 69);
					b2->Visible = true;
					b2->Location = System::Drawing::Point(127, 69);
					sign->Visible = true;
					right_part->Text += L"-1";
					right_part->Location = System::Drawing::Point(173, 50);
					a2->Text = Math::Round(Math::Sqrt(delta / ov[0]) / ov[1], 1) + L"²";
					b2->Text = Math::Round(Math::Sqrt(delta / ov[1]) / ov[0], 1) + L"²";

					label9->Visible = false;
					chart1->Visible = false;
				}

				 
				if (D < 0) {
					curveType->Text = L"гипербола";

					pictureBox12->Visible = true;
					pictureBox12->Location = System::Drawing::Point(52, 35);
					pictureBox14->Visible = true;
					pictureBox14->Location = System::Drawing::Point(139, 30);
					bar1->Visible = true;
					bar1->Location = System::Drawing::Point(48, 64);
					bar2->Visible = true;
					bar2->Location = System::Drawing::Point(135, 64);
					a2->Visible = true;
					a2->Location = System::Drawing::Point(42, 69);
					b2->Visible = true;
					b2->Location = System::Drawing::Point(127, 69);
					sign->Visible = true;
					right_part->Text += L"1";
					right_part->Location = System::Drawing::Point(173, 50);
					sign->Text = L"-";
					if (delta > 0) {
						a2->Text = Math::Round(Math::Sqrt(-delta / ov[1]) / ov[0], 1) + L"²";
						b2->Text = Math::Round(-Math::Sqrt(delta / ov[0]) / ov[1], 1) + L"²";
						AxisMax = int(Math::Sqrt(-delta / ov[1]) / ov[0] + 5);
					}
					else {
						a2->Text = Math::Round(-Math::Sqrt(-delta / ov[0]) / ov[1], 1) + L"²";
						AxisMax = int(-Math::Sqrt(-delta / ov[0]) / ov[1] + 5);
						b2->Text = Math::Round(Math::Sqrt(delta / ov[1]) / ov[0], 1) + L"²";
						std::swap(ovect[0][0], ovect[0][1]);
						std::swap(ovect[1][0], ovect[1][1]);
					}					
					interval = Math::Abs(AxisMax / 5.0);
				}
				if (D == 0) {
					curveType->Text = L"парабола";

					pictureBox14->Visible = true;
					pictureBox14->Location = System::Drawing::Point(48, 64);
					right_part->Text += Math::Round(Math::Sqrt(-delta / ov[0]) / ov[0] * 2, 1) + L"x";
					right_part->Location = System::Drawing::Point(75, 64);
					
					slauMatrix[0][0] = x2;
					slauMatrix[1][1] = y;
					slauMatrix[0][1] = xy / 2;
					slauMatrix[1][0] = x;
					slauMatrix[0][2] = -(x2*x + y*xy)/2 / (x2+y2);
					slauMatrix[1][2] = -c - (x2*x*x + xy*x*y + y2*y*y)/4 / (x2+y2) / (x2+y2);
				}
			}

			if (delta == 0)
			{
				if (D > 0) {
					curveType->Text = L"точка на пересечении двух мнимых прямых (вырожденный эллипс)";

					pictureBox12->Visible = true;
					pictureBox14->Visible = true;
					bar1->Visible = true;
					bar2->Visible = true;
					a2->Visible = true;
					pictureBox12->Visible = true;
					pictureBox12->Location = System::Drawing::Point(52, 35);
					pictureBox14->Visible = true;
					pictureBox14->Location = System::Drawing::Point(139, 30);
					bar1->Visible = true;
					bar1->Location = System::Drawing::Point(48, 64);
					bar2->Visible = true;
					bar2->Location = System::Drawing::Point(135, 64);
					a2->Visible = true;
					a2->Location = System::Drawing::Point(42, 69);
					b2->Visible = true;
					b2->Location = System::Drawing::Point(127, 69);
					sign->Visible = true;
					right_part->Text += L"0";
					right_part->Location = System::Drawing::Point(173, 50);
					b2->Visible = true;					

					a2->Text = Math::Round(Math::Sqrt(ov[1]), 1) + L"²";
					b2->Text = Math::Round(Math::Sqrt(ov[0]), 1) + L"²";
				}
				if (D < 0) {
					curveType->Text = L"пересекающиеся прямые (вырожденная гипербола)";

					pictureBox12->Visible = true;
					pictureBox12->Location = System::Drawing::Point(52, 35);
					pictureBox14->Visible = true;
					pictureBox14->Location = System::Drawing::Point(139, 30);
					bar1->Visible = true;
					bar1->Location = System::Drawing::Point(48, 64);
					bar2->Visible = true;
					bar2->Location = System::Drawing::Point(135, 64);
					a2->Visible = true;
					a2->Location = System::Drawing::Point(42, 69);
					b2->Visible = true;
					b2->Location = System::Drawing::Point(127, 69);
					sign->Visible = true;
					right_part->Text += L"0";
					right_part->Location = System::Drawing::Point(173, 50);
					sign->Text = L"-";

					a2->Text = Math::Round(Math::Sqrt(-ov[1]), 1) + L"²";
					b2->Text = Math::Round(Math::Sqrt(ov[0]), 1) + L"²";
				}
				if (D == 0)
				{
					if (B < 0) {
						curveType->Text = L"параллельные прямые";

						pictureBox14->Visible = true;
						pictureBox14->Location = System::Drawing::Point(48, 50);
						sign->Visible = true;
						sign->Text = L"-";
						b2->Visible = true;
						b2->Location = System::Drawing::Point(115, 50);
						right_part->Text += L"0";
						right_part->Location = System::Drawing::Point(173, 50);

						if (x2 == 0) {
							b2->Text = Math::Round(Math::Sqrt(Math::Abs(-c / y2 - y*y / 4 / y2 / y2)), 1) + L"²";
						}
						else {
							b2->Text = Math::Round(Math::Sqrt(Math::Abs(-c / x2 - x*x / 4 / x2 / x2)), 1) + L"²";
						}
					}
					if (B == 0) {
						curveType->Text = L"прямая (две слившиеся параллельные прямые)";

						pictureBox14->Visible = true;
						pictureBox14->Location = System::Drawing::Point(48, 50);
						right_part->Text += L"0";
						right_part->Location = System::Drawing::Point(95, 50);
					}
					if (B > 0) {
						curveType->Text = L"мнимые параллельные прямые";

						pictureBox14->Visible = true;
						pictureBox14->Location = System::Drawing::Point(48, 50);
						sign->Visible = true;
						sign->Text = L"+";
						b2->Visible = true;
						b2->Location = System::Drawing::Point(115, 50);
						if (x2 == 0) {
							b2->Text = Math::Round(Math::Sqrt(Math::Abs(c / y2 - y*y / 4 / y2 / y2)), 1) + L"²";
						}
						else {
							b2->Text = Math::Round(Math::Sqrt(Math::Abs(c / x2 - x*x / 4 / x2 / x2)), 1) + L"²";
						}
						right_part->Text += L"0";
						right_part->Location = System::Drawing::Point(173, 50);

						label9->Visible = false;
						chart1->Visible = false;
					}
				}
			}
			
			curveType->Size = curveType->GetPreferredSize(Size.Empty);

			Slau <double> slau(2, 2, slauMatrix);
			double center[2] = { 0,0 };
			if (delta != 0) {
				slau.Solve();
				center[0] = slau.solution[0][0];
				center[1] = slau.solution[0][1];
			}

			chart1->ChartAreas[0]->AxisX->ScrollBar->Enabled = false;
			chart1->ChartAreas[0]->AxisY->ScrollBar->Enabled = false;
			
			chart1->Series->Clear();
			Series^ axisX = gcnew Series(L"axisX");
			Series^ axisY = gcnew Series(L"axisY");
			Series^ a1 = gcnew Series(L"a1");
			Series^ a2 = gcnew Series(L"a2");
			Series^ graph = gcnew Series(L"graph");


			graph->Color = Color::Blue;
			axisX->Color = Color::Black;
			axisY->Color = Color::Black;
			a1->Color = Color::Red;
			a2->Color = Color::DarkMagenta;

			graph->ChartType = SeriesChartType::Point;
			axisX->ChartType = SeriesChartType::Line;
			axisY->ChartType = SeriesChartType::Line;
			a1->ChartType = SeriesChartType::Line;
			a2->ChartType = SeriesChartType::Line;
			a2->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Diamond;
			a1->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Diamond;

			a1->Label = L"(#VALX{G2}; #VAL{G2})";
			a1->LabelToolTip = L"a₁";
			a2->Label = L"(#VALX{G2}; #VAL{G2})";
			a2->LabelToolTip = L"a₂";

			graph->MarkerSize = 2;
			axisX->BorderWidth = 3;
			axisY->BorderWidth = 3;
			a1->BorderWidth = 2;
			a2->BorderWidth = 2;

			chart1->Series->Add(axisX);
			chart1->Series->Add(axisY);
			chart1->Series->Add(a1);
			chart1->Series->Add(a2);
			chart1->Series->Add(graph);

			chart1->ChartAreas[0]->AxisX->Minimum = (int)center[0]- AxisMax;
			chart1->ChartAreas[0]->AxisX->Maximum = (int)center[0]+ AxisMax;
			chart1->ChartAreas[0]->AxisX->Interval = interval;

			chart1->ChartAreas[0]->AxisY->Minimum = (int)center[1]- AxisMax;
			chart1->ChartAreas[0]->AxisY->Maximum = (int)center[1]+ AxisMax;
			chart1->ChartAreas[0]->AxisY->Interval = interval;
			
			int numOfResult;
			double result[2];

			if (delta == 0 && D > 0) {
				graph->Points->AddXY(0.00001, 0);
				graph->MarkerSize = 6;
			}			

			if(delta == 0 && D == 0 && (B<0 || B==0) && x2 != 0) {
				graph->ChartType = SeriesChartType::Line;
				graph->BorderWidth = 4;
				graph->Points->AddXY(Math::Sqrt(Math::Abs(-c / x2 - x*x / 4 / x2 / x2)), center[1] - AxisMax);
				graph->Points->AddXY(Math::Sqrt(Math::Abs(-c / x2 - x*x / 4 / x2 / x2)), center[1] + AxisMax +5);
				graph->Points->AddXY(-Math::Sqrt(Math::Abs(-c / x2 - x*x / 4 / x2 / x2)), center[1] + AxisMax + 5);
				graph->Points->AddXY(-Math::Sqrt(Math::Abs(-c / x2 - x*x / 4 / x2 / x2)), center[1] - AxisMax);
			}

			for (double i = (int)center[0] - AxisMax; i <= (int)center[0] + AxisMax; i += 0.001)
			{
				//i = Math::Round(i, 3);
				equation[0] = x2*i*i + x*i + c;
				equation[1] = xy*i + y;
				equation[2] = y2;

				numOfResult = equation.solve(result);
				switch (numOfResult) {
				case -1:
					graph->Points->AddXY(i, result[0]);
					break;
				case 1:
					graph->Points->AddXY(i, result[0]);
					break;
				case 2:
					graph->Points->AddXY(i, result[0]);
					graph->Points->AddXY(i, result[1]);
					break;				
				}
			}
			axisX->Points->AddXY(center[0]- AxisMax-1, 0);
			axisX->Points->AddXY(center[0]+ AxisMax+1, 0);

			axisY->Points->AddXY(0, center[1]- AxisMax-1);
			axisY->Points->AddXY(0, center[1]+ AxisMax+1);
			
			a1->Points->AddXY(center[0], center[1]);
			a1->Points->AddXY(ovect[0][0] + center[0], ovect[1][0] + center[1]);

			a2->Points->AddXY(center[0], center[1]);
			a2->Points->AddXY(ovect[0][1] + center[0], ovect[1][1] + center[1]);
			
		}
		catch (::Exception& ex) {
			MessageBox::Show(wchar_tToSysString(ex.GetMessage()), "Ошибка", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
	}
	catch (System::Exception^ ex) {
		MessageBox::Show(ex->Message, "Ошибка", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}

private: System::Void exitAltF4ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	{
		Application::Exit();		
	}
}

private: System::Void chart1_MouseWheel(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (e->Delta < 0 && Control::ModifierKeys == Keys::Control)
	{
		chart1->ChartAreas[0]->AxisX->ScaleView->ZoomReset();
		chart1->ChartAreas[0]->AxisY->ScaleView->ZoomReset();
	}

	if (e->Delta > 0 && Control::ModifierKeys == Keys::Control)
	{
		double xMin = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMinimum;
		double xMax = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMaximum;
		double yMin = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMinimum;
		double yMax = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMaximum;

		double posXStart = -(xMax - xMin) / 4;
		double posXFinish = (xMax - xMin) / 4;
		double posYStart = -(yMax - yMin) / 4;
		double posYFinish = (yMax - yMin) / 4;

		chart1->ChartAreas[0]->AxisX->ScaleView->Zoom(posXStart, posXFinish);
		chart1->ChartAreas[0]->AxisY->ScaleView->Zoom(posYStart, posYFinish);
	}
}

private: System::Void chart1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	prevPosX = e->X;
	prevPosY = e->Y;
}

private: System::Void chart1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	 double currPosX;
	 double currPosY;

	 chart1->ChartAreas[0]->AxisX->ScaleView->SmallScrollMinSize = 0.001;
	 chart1->ChartAreas[0]->AxisY->ScaleView->SmallScrollMinSize = 0.001;

	 if (e->Button == System::Windows::Forms::MouseButtons::Left) {
		 currPosX = e->X;
		 currPosY = e->Y;
		 if ((chart1->ChartAreas[0]->AxisX->ScaleView->Position - (currPosX - prevPosX) / 15) < chart1->ChartAreas[0]->AxisX->Maximum
			 && (chart1->ChartAreas[0]->AxisX->ScaleView->Position - (currPosX - prevPosX) / 15) > chart1->ChartAreas[0]->AxisX->Minimum)
		 {
			 if ((currPosX - prevPosX) > 0)
				 chart1->ChartAreas[0]->AxisX->ScaleView->Scroll(System::Windows::Forms::DataVisualization::Charting::ScrollType::SmallDecrement);

			 if ((currPosX - prevPosX) < 0)
				 chart1->ChartAreas[0]->AxisX->ScaleView->Scroll(System::Windows::Forms::DataVisualization::Charting::ScrollType::SmallIncrement);
		 }
		 if ((chart1->ChartAreas[0]->AxisY->ScaleView->Position + (currPosY - prevPosY) / 15) < chart1->ChartAreas[0]->AxisY->Maximum
			 && (chart1->ChartAreas[0]->AxisY->ScaleView->Position + (currPosY - prevPosY) / 15) > chart1->ChartAreas[0]->AxisY->Minimum)
		 {
			 if ((currPosY - prevPosY) < 0)
				 chart1->ChartAreas[0]->AxisY->ScaleView->Scroll(System::Windows::Forms::DataVisualization::Charting::ScrollType::SmallDecrement);

			 if ((currPosY - prevPosY) > 0)
				 chart1->ChartAreas[0]->AxisY->ScaleView->Scroll(System::Windows::Forms::DataVisualization::Charting::ScrollType::SmallIncrement);
		 }
		 prevPosX = currPosX;
		 prevPosY = currPosY;
		 
	 }

 }

private: System::Void MainWnd_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	if (e->KeyCode == Keys::F1)
	{
		AboutDlg^ about = gcnew AboutDlg;
		about->ShowDialog(this);
	}
}

private: System::Void MainWnd_Load(System::Object^  sender, System::EventArgs^  e) {
	this->ClientSize = System::Drawing::Size(START_WIDTH, START_HEIGHT);
	this->Location = Point((Screen::PrimaryScreen->Bounds.Width - START_WIDTH) / 2,
		(Screen::PrimaryScreen->Bounds.Height - START_HEIGHT) / 2);
}

private: System::Void textBox1_Enter(System::Object^  sender, System::EventArgs^  e) {
	if (!System::String::IsNullOrEmpty(textBox1->Text))
	{
		textBox1->SelectionStart = 0;
		textBox1->SelectionLength = textBox1->Text->Length;
	}
}

private: System::Void textBox2_Enter(System::Object^  sender, System::EventArgs^  e) {
	if (!System::String::IsNullOrEmpty(textBox2->Text))
	{
		textBox2->SelectionStart = 0;
		textBox2->SelectionLength = textBox2->Text->Length;
	}
}

private: System::Void textBox3_Enter(System::Object^  sender, System::EventArgs^  e) {
	if (!System::String::IsNullOrEmpty(textBox3->Text))
	{
		textBox3->SelectionStart = 0;
		textBox3->SelectionLength = textBox3->Text->Length;
	}
}

private: System::Void textBox4_Enter(System::Object^  sender, System::EventArgs^  e) {
	if (!System::String::IsNullOrEmpty(textBox4->Text))
	{
		textBox4->SelectionStart = 0;
		textBox4->SelectionLength = textBox4->Text->Length;
	}
}

private: System::Void textBox5_Enter(System::Object^  sender, System::EventArgs^  e) {
	if (!System::String::IsNullOrEmpty(textBox5->Text))
	{
		textBox5->SelectionStart = 0;
		textBox5->SelectionLength = textBox5->Text->Length;
	}
}

private: System::Void textBox6_Enter(System::Object^  sender, System::EventArgs^  e) {
	if (!System::String::IsNullOrEmpty(textBox6->Text))
	{
		textBox6->SelectionStart = 0;
		textBox6->SelectionLength = textBox6->Text->Length;
	}
}

private: System::Void example_Click(System::Object^  sender, System::EventArgs^  e) {
	textBox1->Text = L"9";
	textBox2->Text = L"6";
	textBox3->Text = L"4";
	textBox4->Text = L"-16";
	textBox5->Text = L"-8";
	textBox6->Text = L"-2";
	solve_Click(sender, e);
	this->Location = Point(155, 90);
	Instruction^ instruction = gcnew Instruction;
	instruction->Show();
}

private: System::Boolean isNumber(System::Windows::Forms::KeyEventArgs^  e) {

	if (e->KeyCode >= Keys::D0 && e->KeyCode <= Keys::D9) return true;

	if (e->KeyCode >= Keys::NumPad0 && e->KeyCode <= Keys::NumPad9) return true;

	return false;
}			

private: System::Void ownVect1_2_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	try {
		if (isNumber(e)) {
			double Y = System::Double::Parse(ownVect1_2->Text);
			if (Y == 0) return;
			double X = OV1X / (OV1Y / Y);
			OV1X = X;
			OV1Y = Y;
			ownVect1_1->Text = X.ToString();
			ownVect1_2->Text = Y.ToString();
		}
	}
	catch (System::Exception^ ex) {
		MessageBox::Show(ex->Message, "Ошибка", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}

private: System::Void textBox12_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	try {
		if (isNumber(e)) {
			double Y = System::Double::Parse(textBox12->Text);
			if (Y == 0) return;
			double X = OV2X / (OV2Y / Y);
			OV2X = X;
			OV2Y = Y;
			textBox11->Text = X.ToString();
			textBox12->Text = Y.ToString();
		}
	}
	catch (System::Exception^ ex) {
		MessageBox::Show(ex->Message, "Ошибка", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}

private: System::Void ownVect2_2_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	try {
		if (isNumber(e)) {
			double Y = System::Double::Parse(ownVect2_2->Text);
			if (Y == 0) return;
			double X = OV2X / (OV2Y / Y);
			OV2X = X;
			OV2Y = Y;
			ownVect2_1->Text = X.ToString();
			ownVect2_2->Text = Y.ToString();
		}
	}
	catch (System::Exception^ ex) {
		MessageBox::Show(ex->Message, "Ошибка", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}

};
}