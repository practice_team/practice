#pragma once
#include <iostream>
#include "exception.h"
#include "binomial.h"
#include "matrix.h"

template <class T> class SquareMatrix {
protected:
	T** matrix;

	const unsigned size = 2;
	const double   eps = 0.00000000001;
public:
	SquareMatrix(T**);
	SquareMatrix();
	SquareMatrix(const SquareMatrix&);

	~SquareMatrix();

	T             determinant();
	Matrix <T>    subMatrix(unsigned rowIndex, unsigned columnIndex);
	Binomial <T>  �haracteristicEquation();
	int           ownValues(T* conatainer);
	SquareMatrix  getStepForm();
	unsigned      rank();
	SquareMatrix  getOwnVectors(T first = 1);      // first - ����������� ����� ����������� ��������
	SquareMatrix  getOrthoNormaliziedOwnVectors();
	void          roud();


	T*            operator [] (unsigned     );
	SquareMatrix  operator +  (SquareMatrix&);
	SquareMatrix  operator *  (SquareMatrix&);
	SquareMatrix  operator *  (T            );
	SquareMatrix& operator =  (SquareMatrix&);
	SquareMatrix  operator !  (             );  // �������� ��������� ����������������� �������
	SquareMatrix  operator ~  (             );  // �������� ��������� �������� �������

	template <class T> friend SquareMatrix <T> operator *  (T            , SquareMatrix<T>&);
	template <class T> friend std::ostream&    operator << (std::ostream&, SquareMatrix<T>&);
	template <class T> friend std::istream&    operator >> (std::istream&, SquareMatrix<T>&);
};