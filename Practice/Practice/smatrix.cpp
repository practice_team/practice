#include "smatrix.h"
#include <iomanip>

template <class T> SquareMatrix <T>::SquareMatrix(T** _matrix) {

	matrix = new T*[size];

	for (unsigned i = 0; i < size; i++) {
		matrix[i] = new T[size];
	}

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = _matrix[i][j];
		}
	}

}

template <class T> SquareMatrix <T>::SquareMatrix() {
	matrix = new T*[size];

	for (unsigned i = 0; i < size; i++) {
		matrix[i] = new T[size];
	}

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = 0;
		}
	}
}

template <class T> SquareMatrix <T>::SquareMatrix(const SquareMatrix <T>& ob) {
	matrix = new T*[size];

	for (unsigned i = 0; i < size; i++) {
		matrix[i] = new T[size];
	}

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = ob.matrix[i][j];
		}
	}

}

template <class T> SquareMatrix <T>::~SquareMatrix() {
	for (unsigned i = 0; i < size; i++) {
		delete[] matrix[i];
	}

	delete[] matrix;
}

template <class T> std::istream& operator >> (std::istream& in, SquareMatrix <T>& ob) {
	for (unsigned i = 0; i < ob.size; i++) {
		for (unsigned j = 0; j < ob.size; j++) {
			in >> ob[i][j];
		}
	}

	return in;
}

template <class T> std::ostream& operator << (std::ostream& out, SquareMatrix <T>& ob) {
	for (unsigned i = 0; i < ob.size; i++) {
		for (unsigned j = 0; j < ob.size; j++) {
			out << std::setw(12) << ob[i][j];
		}
		out << std::endl;
	}

	return out;
}

template <class T> T* SquareMatrix <T>::operator [] (unsigned i) {
	if (i < 0 || i > size - 1) {
		throw ::BadIndexException();
	}

	return matrix[i];
}

template <class T> SquareMatrix <T> SquareMatrix <T>::operator + (SquareMatrix <T>& ob) {
	SquareMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp.matrix[i][j] = matrix[i][j] + ob.matrix[i][j];
		}
	}

	return temp;
}

template <class T> SquareMatrix <T> SquareMatrix <T>::operator * (SquareMatrix <T>& ob) {
	SquareMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < ob.size; j++) {
			for (int k = 0; k < size; k++) {
				temp.matrix[i][j] =
					temp.matrix[i][j] + matrix[i][k] * ob.matrix[k][j];
			}
		}
	}

	return temp;
}

template <class T> SquareMatrix <T> SquareMatrix <T>::operator * (T number) {
	SquareMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp[i][j] = matrix[i][j] * number;
		}
	}

	return temp;
}

template <class T> SquareMatrix <T> operator * (T number, SquareMatrix <T>& ob) {
	SquareMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp[i][j] = number * ob[i][j];
		}
	}

	return temp;
}

template <class T> SquareMatrix <T>& SquareMatrix <T>::operator = (SquareMatrix <T>& ob) {
	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = ob.matrix[i][j];
		}
	}

	return *this;
}

template <class T> SquareMatrix <T> SquareMatrix <T>::operator ! () {
	SquareMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp.matrix[j][i] = matrix[i][j];
		}
	}

	return temp;
}

template <class T> SquareMatrix <T> SquareMatrix <T>::operator ~ () {
	SquareMatrix <T> res(size);
	T det = determinant();

	if (det == T(0)) {
		throw ::ZeroDivideException();
	}

	SquareMatrix <T> temp(size);
	int z;

	for (int i = 0; i < size; i++) {
		z = (i % 2 == 0) ? 1 : -1;

		for (int j = 0; j < size; j++) {
			temp = subMatrix(i, j);
			res[j][i] = z * temp.determinant() / det;
			z = -z;
		}
	}

	return res;
}

template <class T> Matrix <T> SquareMatrix <T>::subMatrix(unsigned rowIndex, unsigned columnIndex) {
	if (rowIndex < 0 || rowIndex > size - 1) {
		throw ::BadIndexException();
	}

	if (columnIndex < 0 || columnIndex > size - 1) {
		throw ::BadIndexException();
	}

	Matrix <T> temp(size - 1, size - 1);

	for (unsigned i = 0; i < rowIndex; i++) {
		for (unsigned j = 0; j < columnIndex; j++) {
			temp[i][j] = matrix[i][j];
		}

		for (unsigned j = columnIndex + 1; j < size; j++) {
			temp[i][j - 1] = matrix[i][j];
		}
	}

	for (unsigned i = rowIndex + 1; i < size; i++) {
		for (unsigned j = 0; j < columnIndex; j++) {
			temp[i - 1][j] = matrix[i][j];
		}

		for (unsigned j = columnIndex + 1; j < size; j++) {
			temp[i - 1][j - 1] = matrix[i][j];
		}
	}

	return temp;
}

template <class T> T SquareMatrix <T>::determinant() {
	T det = 0;

	Matrix <T> temp;

	for (unsigned j = 0; j < size; j++) {
		temp = subMatrix(0, j);

		if (j % 2 == 0) {
			det = det + temp.determinant() * matrix[0][j];
		}
		else {
			det = det - temp.determinant() * matrix[0][j];
		}
	}

	return det;
}

template <class T> SquareMatrix <T> SquareMatrix <T>::getStepForm() {
	SquareMatrix <T> temp = *this;
	unsigned iMax;

	for (unsigned i = 0; i < size - 1; i++) {
		// ������� ������ � ������������ �� ������ ������ ���������
		iMax = i;
		for (unsigned j = i + 1; j < size; j++) {
			if (abs(temp[j][i]) > abs(temp[iMax][i])) {
				iMax = j;
			}
		}

		if (abs(temp[iMax][i]) < eps) {
			continue;
		}

		for (unsigned j = 0; j < size; j++) {
			std::swap(temp[i][j], temp[iMax][j]);
		}

		//  �������� ������� ������ �� ���� ���������
		for (unsigned j = i + 1; j < size; j++) {
			T q = -temp[j][i] / temp[i][i];
			for (unsigned k = i; k < size; k++) {
				temp[j][k] += q * temp[i][k];
			}
		}
	}

	temp.roud();

	return temp;
}

template <class T> unsigned SquareMatrix <T>::rank() {
	SquareMatrix <T> temp = getStepForm();
	unsigned result = 0;

	for (unsigned i = 0; i < size; i++) {
		if (temp[i][i] != 0) {
			result++;
		}
	}

	return result;
}

template <class T> void SquareMatrix <T>::roud() {
	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			if (abs(matrix[i][j]) < eps) {
				matrix[i][j] = 0;
			}
		}
	}
}

template <class T> Binomial <T> SquareMatrix <T>::�haracteristicEquation() {
	SquareMatrix <Binomial <T>> temp;
	Binomial <T> diagonalMonom(T(0), T(-1));

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			if (i == j) {
				diagonalMonom[0] = matrix[i][j];
				temp[i][j] = diagonalMonom;
			}
			else {
				temp[i][j] = matrix[i][j];
			}
		}
	}

	return temp.determinant();
}

template <class T> int SquareMatrix <T>::ownValues(T* conatainer) {
	Binomial <T> �harEquation = �haracteristicEquation();
	return �harEquation.solve(conatainer);
}

template <class T> SquareMatrix <T> SquareMatrix <T>::getOwnVectors(T first) {
	SquareMatrix <T> temp = *this;
	SquareMatrix <T> result;

	T* ownVal = new T[size];
	if (ownValues(ownVal) == 0) {
		throw ::ComplexOwnValuesException();
	}
	//��� �������
	if (ownVal[0] != ownVal[1]) {
		for (unsigned k = 0; k < size; k++) {
			for (unsigned j = 0; j < size; j++) {
				temp[j][j] -= ownVal[k];
			}

			temp = temp.getStepForm();

			if (temp[0][0] == 0) {
				result[0][k] = 1;
				result[1][k] = 0;
			}
			else {
				result[0][k] = -temp[0][1] * first / temp[0][0];
				result[1][k] = first;
			}

			temp = *this;
		}

		result.roud();
		return result;
	}

	//��� �������
	for (unsigned i = 0; i < size; i++) {
		result[i][i] = 1;
	}

	result.roud();
	return result;
}

template <class T> SquareMatrix <T> SquareMatrix <T>::getOrthoNormaliziedOwnVectors() {
	SquareMatrix <T> result = getOwnVectors();
	T& a1 = result[0][0];
	T& a2 = result[0][1];
	T& b1 = result[1][0];
	T& b2 = result[1][1];

	T sqrt1 = sqrt(a1*a1 + a2*a2);
	T sqrt2 = sqrt(b1*b1 + b2*b2);

	a1 /= sqrt1;
	result[0][0];
	a2 /= sqrt1;
	b1 /= sqrt2;
	b2 /= sqrt2;

	return result;
}