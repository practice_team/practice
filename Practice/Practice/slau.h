#pragma once
#include "matrix.h"
#include <iostream>

template <class T> class Slau
{
	unsigned   equationCount;  // ���������� ���������
	unsigned   variableCount;  // ���������� ����������
	
	Matrix <T>     rightPart;  // ������ ��������� ������ �����
	bool            isSolved;  // ����������, �������� �� ������� ����������
	unsigned*         reoder;  // ������ ������������ ����������
	unsigned            rang;  // ���� �������

public:
	Matrix<T>      solution;  // ������� ������� �������� ���������
	Matrix <T>  coefficients;  // ������� �������������
	Slau(unsigned _equationCount, unsigned _variableCount);
	Slau(unsigned _equationCount, unsigned _variableCount, T** matrix);
	
	void Solve();                      // ������� ������ ������ ������� ����     
	void Kramer();                     // �������, ����������� ����� �������
	void InverseMatrix();              // �������, ����������� ������� ����,� ������� �������� �������
	void JordanGauss();                // ������� ��������� ������ ������� ����, ������� �������-������
	void PrintSolution(std::ostream&); // ����� ������ ���������� ��� ������� ����

	template <class T> friend std::ostream& operator << (std::ostream&, Slau<T>&); 
	template <class T> friend std::istream& operator >> (std::istream&, Slau<T>&);
};