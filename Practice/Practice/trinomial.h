#pragma once

#include <iostream>
#include <vector>
#include <algorithm>

template <class T> class Trinomial {
protected:
	const double eps      = 0.00000000001;
	const double TwoPi    = 6.28318530717958648;
	const double SolveEps = 1e-14;

	std::vector <T>  coeff;
	Trinomial <T>&   minus_offset(const Trinomial <T> &);
	void             balance();

public:
	//Constructors
	Trinomial(T x0 = 0);

	Trinomial(T, T);

	Trinomial(T, T, T);

	Trinomial(T, T, T, T);

	Trinomial(T *arg, unsigned count);
	
	//Methods
	T            value  (T   ) const;
	unsigned int degree (    ) const;
	int          solve  (T*  ) const;
	unsigned int size   (    ) const;

	//Operators

	T&         operator [] (const unsigned  );
	T          operator [] (const unsigned  ) const;

	Trinomial& operator =  (const Trinomial&);
	Trinomial& operator =  (const T&        );

	Trinomial& operator += (const Trinomial&);
	Trinomial& operator += (const T&        );

	Trinomial& operator -= (const Trinomial&);
	Trinomial& operator -= (const T&        );

	Trinomial& operator *= (const Trinomial&);
	Trinomial& operator *= (const T&        );


	template <class T> friend Trinomial      operator -  (const Trinomial&                  );
	template <class T> friend std::ostream&  operator << (std::ostream&   , const Trinomial&);
	template <class T> friend std::istream&  operator >> (std::istream&   , const Trinomial&);
									 
	template <class T> friend Trinomial      operator +  (const Trinomial&, const Trinomial&);
	template <class T> friend Trinomial      operator +  (const Trinomial&, const T&        );
	template <class T> friend Trinomial      operator +  (const T&        , const Trinomial&);
									         
	template <class T> friend Trinomial      operator -  (const Trinomial&, const Trinomial&);
	template <class T> friend Trinomial      operator -  (const Trinomial&, const T&        );
	template <class T> friend Trinomial      operator -  (const T&        , const Trinomial&);
									         
	template <class T> friend Trinomial      operator *  (const Trinomial&, const Trinomial&);
	template <class T> friend Trinomial      operator *  (const Trinomial&, const T&        );
	template <class T> friend Trinomial      operator *  (const T&        , const Trinomial&);
};