#pragma once

#include <iostream>

class Exception {                     
public:
	virtual void ShowMessage() = 0;
	virtual wchar_t* GetMessage() = 0;
};

class BadDimensionException : public ::Exception {						     						   
public:																		 
	void ShowMessage()
	{
		std::cout << "The dimensions of the matrix are incorrect.";
		std::cout << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"������� ������� �������� �������������.";
	}
};

class BadIndexException : public ::Exception {								
public:																		
	void ShowMessage()
	{
		std::cout << "Element with such indixes in the matrix does not exist.";
		std::cout << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"������� � ������ ��������� � ������� �� ����������.";
	}
};

class DimensionSumException : public ::Exception {		                    
public:													                    
	void ShowMessage()
	{
		std::cout << "Dimension summable matrices must be the same";
		std::cout << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"������ ����������� ������ ������ ���� ����������.";
	}
};

class DimensionProductException : public ::Exception {		                 
public:														                 
	void ShowMessage()
	{
		std::cout << "The number of columns of the first matrix";
		std::cout << "must match the number of rows of the second matrix.";
		std::cout << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"����� �������� ������ ������� ������ ��������� � ����������� ����� ������ �������.";
	}
};

class ZeroDivideException : public ::Exception {					          
public:															             
	void ShowMessage()
	{
		std::cout << "Divide by 0." << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"������� �� 0.";
	}
};

class NonSquareMatrixException : public ::Exception {
public:
	void ShowMessage() {
		std::cout << "Matrix is not square." << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"������� �� �������� ����������.";
	}
};

class ComplexOwnValuesException : public ::Exception {
public:
	void ShowMessage() {
		std::cout << "Complex own values." << std::endl;
	}

	wchar_t* GetMessage()
	{
		return L"���������� ����������� ��������.";
	}
};