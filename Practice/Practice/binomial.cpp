#include "binomial.h"
#include "common.h"

template <class T> Binomial <T>::Binomial(T x0) {
	coeff.push_back(x0);
	coeff.push_back(0);
	coeff.push_back(0);
}

template <class T> Binomial <T>::Binomial(T x0, T x1) {
	coeff.push_back(x0);
	coeff.push_back(x1);
	coeff.push_back(0);
}

template <class T> Binomial <T>::Binomial(T x0, T x1, T x2) {
	coeff.push_back(x0);
	coeff.push_back(x1);
	coeff.push_back(x2);
}

template <class T> Binomial <T>::Binomial(T* arg, unsigned count) {
	for (unsigned i = 0; i < count; i++) {
		coeff.push_back(arg[i]);
	}
}

template <class T> T Binomial <T>::value(T x) const {
	T cur = 1, res = 0;
	for (unsigned i = 0; i < coeff.size(); i++) {
		res += cur * coeff[i];
		cur *= x;
	}
	return res;
}

template <class T> void Binomial <T>::balance() {
	unsigned x;

	for (x = coeff.size() - 1; x > 0; x--) {
		if (abs(coeff[x] - T(0)) >= eps) {
			break;
		}
	}

	coeff.resize(x + 1);

	if (coeff.size() == 0) {
		coeff.push_back(0);
	}
}

template <class T> unsigned int Binomial <T>::degree() const {
	return coeff.size() - 1;
}

template <class T> unsigned int Binomial <T>::size() const {
	return coeff.size();
}

template <class T> T& Binomial <T>::operator [] (const unsigned arg) {
	return coeff[arg];
}

template <class T> T Binomial <T>::operator [] (const unsigned arg) const {
	return coeff[arg];
}

template <class T> Binomial <T>& Binomial <T>::operator = (const Binomial <T>& arg) {
	coeff.resize(arg.coeff.size());

	for (unsigned i = 0; i < coeff.size(); i++) {
		coeff[i] = arg.coeff[i];
	}

	return *this;
}

template <class T> Binomial <T>& Binomial <T>::operator = (const T& arg) {
	coeff.resize(1);
	coeff[0] = arg;

	return *this;
}

template <class T> Binomial <T>& Binomial <T>::operator += (const Binomial <T>& arg) {
	unsigned m = std::min(coeff.size(), arg.coeff.size());

	for (unsigned i = 0; i < m; i++) {
		coeff[i] += arg.coeff[i];
	}

	for (unsigned i = m; i < arg.coeff.size(); i++) {
		coeff.push_back(arg.coeff[i]);
	}

	balance();

	return *this;
}

template <class T> Binomial <T>& Binomial <T>::operator += (const T& arg) {
	coeff[0] += arg;

	return *this;
}

template <class T> Binomial <T>& Binomial <T>::operator -= (const Binomial <T>& arg) {
	unsigned m = std::min(coeff.size(), arg.coeff.size());

	for (unsigned i = 0; i < m; i++) {
		coeff[i] -= arg.coeff[i];
	}

	for (unsigned i = m; i < arg.coeff.size(); i++) {
		coeff.push_back(-arg.coeff[i]);
	}

	balance();

	return *this;
}

template <class T> Binomial <T>& Binomial <T>::operator -= (const T& arg) {
	coeff[0] -= arg;

	balance();

	return *this;
}

template <class T> Binomial <T>& Binomial <T> ::operator *= (const Binomial <T>& arg) {
	int old_size = coeff.size();

	coeff.resize(old_size + arg.coeff.size() + 1);

	for (int i = coeff.size() - 1; i >= old_size; i--) {
		coeff[i] = 0;
	}

	for (int i = old_size - 1; i >= 0; i--) {
		for (int j = arg.coeff.size() - 1; j > 0; j--) {
			coeff[i + j] += coeff[i] * arg.coeff[j];
		}
		coeff[i] = coeff[i] * arg.coeff[0];
	}

	balance();

	return *this;
}

template <class T> Binomial <T>& Binomial <T> ::operator *= (const T& arg) {
	coeff[0] *= arg;

	balance();

	return *this;
}

template <class T> Binomial <T>& Binomial <T>::minus_offset(const Binomial <T>& arg) {
	if (arg.coeff.size() > coeff.size()) {
		return *this;
	}

	for (int i = 1; i <= arg.coeff.size(); i++) {
		coeff[coeff.size() - i] -= arg.coeff[arg.coeff.size() - i];
	}

	balance();

	return *this;
}


template <class T> Binomial <T>  operator - (const Binomial <T>& arg) {
	Binomial <T>  res;
	res.coeff.resize(arg.coeff.size());

	for (int i = 0; i < res.coeff.size(); i++) {
		res.coeff[i] = -arg.coeff[i];
	}

	return res;
}


template <class T> std::ostream& operator << (std::ostream& out, const Binomial <T>& arg) {
	out << "{";
	for (unsigned i = 0; i < arg.size() - 1; i++) {
		out << arg[i] << ",";
	}

	out << arg[arg.size() - 1] << "}";

	return out;
}

template <class T> std::istream& operator >> (std::istream& in, Binomial <T>& arg) {
	char c;

	do {
		in >> c;
	} while (isspace(c));

	if (c != '{') {
		in.exceptions(std::istream::badbit);
		return in;
	}

	arg.coeff.clear();
	for (int i = 0; true; i++) {
		T x;
		in >> x;

		arg.coeff.push_back(x);

		do {
			in >> c;
		} while (isspace(c));

		if (c == '}') {
			break;
		}

		if (c != ',') {
			in.exceptions(std::istream::badbit);
			return in;
		}
	}

	arg.balance();
	return in;
}


template <class T> Binomial <T>  operator+ (const Binomial <T>& a, const Binomial <T>& b) {
	Binomial <T>  c = a;
	c += b;
	return c;
}

template <class T> Binomial <T>  operator+ (const Binomial <T>& a, const T& b) {
	Binomial <T>  c = a;
	c += b;
	return c;
}

template <class T> Binomial <T>  operator+ (const T& a, const Binomial <T>& b) {
	Binomial <T>  c = b;
	c += a;
	return c;
}

template <class T> Binomial <T>  operator- (const Binomial <T>& a, const Binomial <T>& b) {
	Binomial <T>  c = a;
	c -= b;
	return c;
}

template <class T> Binomial <T>  operator- (const Binomial <T>& a, const T& b) {
	Binomial <T>  c = a;
	c -= b;
	return c;
}

template <class T> Binomial <T>  operator- (const T& a, const Binomial <T>& b) {
	Binomial <T>  c = b;
	c -= a;
	return c;
}

template <class T> Binomial <T>  operator* (const Binomial <T>& a, const Binomial <T>& b) {
	Binomial <T>  c = a;
	c *= b;
	return c;
}

template <class T> Binomial <T>  operator* (const Binomial <T>& a, const T& b) {
	Binomial <T>  c = a;
	c *= b;
	return c;
}

template <class T> Binomial <T>  operator* (const T& a, const Binomial <T>& b) {
	Binomial <T>  c = b;
	c *= a;
	return c;
}

template <class T> int Binomial <T>::solve(T *x) const {
	T a = coeff[2];
	T b = coeff[1];
	T c = coeff[0];

	if (a == 0.0) {
		if (b == 0.0) {
			if (c == 0.0) {
				return -3;
			}
			return -2;
		}
		x[0] = -c / b; 
		return -1;
	}

	T d = b*b - 4.*a*c; 
							
	if (d == 0.0) {
		x[0] = x[1] = -b / (2.*a);
		return 1;
	}

	if (d < 0.0) {
		T t = 0.5 / a;
		x[0] = -b*t;
		x[1] = sqrt(-d)*t;
		return 0;
	}

	if (b >= 0) {
		d = (-0.5)*(b + sqrt(d));
	}
	else {
		d = (-0.5)*(b - sqrt(d));
	}

	x[0] = d / a; 
	x[1] = c / d;

	bubble_sort(x, 2);

	return 2;
}