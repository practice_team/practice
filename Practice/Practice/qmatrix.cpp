#include "qmatrix.h"
#include <iomanip>

template <class T> QubicMatrix <T>::QubicMatrix(T** _matrix) {

	matrix = new T*[size];

	for (unsigned i = 0; i < size; i++) {
		matrix[i] = new T[size];
	}

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = _matrix[i][j];
		}
	}

}

template <class T> QubicMatrix <T>::QubicMatrix() {
	matrix = new T*[size];

	for (unsigned i = 0; i < size; i++) {
		matrix[i] = new T[size];
	}

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = 0;
		}
	}
}

template <class T> QubicMatrix <T>::QubicMatrix(const QubicMatrix <T>& ob) {
	matrix = new T*[size];

	for (unsigned i = 0; i < size; i++) {
		matrix[i] = new T[size];
	}

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = ob.matrix[i][j];
		}
	}

}

template <class T> QubicMatrix <T>::~QubicMatrix() {
	for (unsigned i = 0; i < size; i++) {
		delete[] matrix[i];
	}

	delete[] matrix;
}

template <class T> std::istream& operator >> (std::istream& in, QubicMatrix <T>& ob) {
	for (unsigned i = 0; i < ob.size; i++) {
		for (unsigned j = 0; j < ob.size; j++) {
			in >> ob[i][j];
		}
	}

	return in;
}

template <class T> std::ostream& operator << (std::ostream& out, QubicMatrix <T>& ob) {
	for (unsigned i = 0; i < ob.size; i++) {
		for (unsigned j = 0; j < ob.size; j++) {
			out << std::setw(12) << ob[i][j];
		}
		out << std::endl;
	}

	return out;
}

template <class T> T* QubicMatrix <T>::operator [] (unsigned i) {
	if (i < 0 || i > size-1) {
		throw ::BadIndexException();
	}

	return matrix[i];
}

template <class T> QubicMatrix <T> QubicMatrix <T>::operator + (QubicMatrix <T>& ob) {
	QubicMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp.matrix[i][j] = matrix[i][j] + ob.matrix[i][j];
		}
	}

	return temp;
}

template <class T> QubicMatrix <T> QubicMatrix <T>::operator * (QubicMatrix <T>& ob) {
	QubicMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < ob.size; j++) {
			for (int k = 0; k < size; k++) {
				temp.matrix[i][j] =
					temp.matrix[i][j] + matrix[i][k] * ob.matrix[k][j];
			}
		}
	}

	return temp;
}

template <class T> QubicMatrix <T> QubicMatrix <T>::operator * (T number) {
	QubicMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp[i][j] = matrix[i][j] * number;
		}
	}

	return temp;
}

template <class T> QubicMatrix <T> operator * (T number, QubicMatrix <T>& ob) {
	QubicMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp[i][j] = number * ob[i][j];
		}
	}

	return temp;
}

template <class T> QubicMatrix <T>& QubicMatrix <T>::operator = (QubicMatrix <T>& ob) {
	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			matrix[i][j] = ob.matrix[i][j];
		}
	}

	return *this;
}

template <class T> QubicMatrix <T> QubicMatrix <T>::operator ! () {
	QubicMatrix <T> temp(size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			temp.matrix[j][i] = matrix[i][j];
		}
	}

	return temp;
}

template <class T> QubicMatrix <T> QubicMatrix <T>::operator ~ () {
	QubicMatrix <T> res(size);
	T det = determinant();

	if (det == T(0)) {
		throw ::ZeroDivideException();
	}

	QubicMatrix <T> temp(size);
	int z;

	for (int i = 0; i < size; i++) {
		z = (i % 2 == 0) ? 1 : -1;

		for (int j = 0; j < size; j++) {
			temp = subMatrix(i, j);
			res[j][i] = z * temp.determinant() / det;
			z = -z;
		}
	}

	return res;
}

template <class T> Matrix <T> QubicMatrix <T>::subMatrix(unsigned rowIndex, unsigned columnIndex) {
	if (rowIndex < 0 || rowIndex > size-1) {
		throw ::BadIndexException();
	}

	if (columnIndex < 0 || columnIndex > size-1) {
		throw ::BadIndexException();
	}

	Matrix <T> temp(size - 1, size - 1);

	for (unsigned i = 0; i < rowIndex; i++) {
		for (unsigned j = 0; j < columnIndex; j++) {
			temp[i][j] = matrix[i][j];
		}

		for (unsigned j = columnIndex + 1; j < size; j++) {
			temp[i][j - 1] = matrix[i][j];
		}
	}

	for (unsigned i = rowIndex + 1; i < size; i++) {
		for (unsigned j = 0; j < columnIndex; j++) {
			temp[i - 1][j] = matrix[i][j];
		}

		for (unsigned j = columnIndex + 1; j < size; j++) {
			temp[i - 1][j - 1] = matrix[i][j];
		}
	}

	return temp;
}

template <class T> T QubicMatrix <T>::determinant() {
	T det = 0;

	Matrix <T> temp;

	for (unsigned j = 0; j < size; j++) {
		temp = subMatrix(0, j);

		if (j % 2 == 0) {
			det = det + temp.determinant() * matrix[0][j];
		}
		else {
			det = det - temp.determinant() * matrix[0][j];
		}
	}

	return det;
}

template <class T> Trinomial <T> QubicMatrix <T>::�haracteristicEquation() {
	QubicMatrix <Trinomial <T>> temp;
	Trinomial <T> diagonalMonom(T(0), T(-1));

	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			if (i == j) {
				diagonalMonom[0] = matrix[i][j];
				temp[i][j] = diagonalMonom;
			}
			else {
				temp[i][j] = matrix[i][j];
			}
		}
	}

	return temp.determinant();
}

template <class T> int QubicMatrix <T>::ownValues(T* conatainer) {
	Trinomial <T> �harEquation = �haracteristicEquation();
	return �harEquation.solve(conatainer);
}

template <class T> QubicMatrix <T> QubicMatrix <T>::getStepForm() {
	QubicMatrix <T> temp = *this;
	unsigned iMax;

	for (unsigned i = 0; i < size - 1; i++) {
		// ������� ������ � ������������ �� ������ ������ ���������
		iMax = i;
		for (unsigned j = i + 1; j < size; j++) {
			if (abs(temp[j][i]) > abs(temp[iMax][i])) {
				iMax = j;
			}
		}

		if (abs(temp[iMax][i]) < eps) {
			continue;
		}

		for (unsigned j = 0; j < size; j++) {
			swap(temp[i][j], temp[iMax][j]);
		}

		//  �������� ������� ������ �� ���� ���������
		for (unsigned j = i + 1; j < size; j++) {
			T q = -temp[j][i] / temp[i][i];
			for (unsigned k = i; k < size; k++) {
				temp[j][k] += q * temp[i][k];
			}
		}
	}

	temp.roud();

	return temp;
}

template <class T> unsigned QubicMatrix <T>::rank() {
	QubicMatrix <T> temp = getStepForm();
	unsigned result = 0;

	for (unsigned i = 0; i < size; i++) {
		if (temp[i][i] != 0) {
			result++;
		}
	}

	return result;
}

template <class T> QubicMatrix <T> QubicMatrix <T>::getOwnVectors(T first = 1, T second = 1) {
	QubicMatrix <T> temp = *this;
	QubicMatrix <T> result;
	T x1, x2;

	T* ownVal = new T[size];
	if(ownValues(ownVal) == 1) {
		throw ::ComplexOwnValuesException();
	}

	// ��� �������
	if (ownVal[0] != ownVal[1] && ownVal[1] != ownVal[2]) {
		for (unsigned k = 0; k < size; k++) {
			for (unsigned j = 0; j < size; j++) {
				temp[j][j] -= ownVal[k];
			}

			temp = temp.getStepForm();

			x2 = -temp[1][2] * second / temp[1][1];
			x1 = -(temp[0][1]*x2 + temp[0][2] * second) / temp[0][0];

			result[0][k] = x1;
			result[1][k] = x2;
			result[2][k] = second;

			temp = *this;
		}

		result.roud();
		return result;
	}

	// ��� �������
	if (ownVal[0] == ownVal[1] && ownVal[1] == ownVal[2]) {
		for (unsigned i = 0; i < size; i++) {
			result[i][i] = 1;
		}

		result.roud();
		return result;
	}

	// ������ 2 �������
	if (ownVal[0] == ownVal[1]) {
		for (unsigned j = 0; j < size; j++) {
			temp[j][j] -= ownVal[0];
		}

		temp = temp.getStepForm();

		result[0][0] = -temp[0][1] * first / temp[0][0];
		result[1][0] = first;
		result[2][0] = 0;

		result[0][1] = -temp[0][2] * first / temp[0][0];;
		result[1][1] = 0;
		result[2][1] = second;

		/////////////////////////////////////

		temp = *this;

		for (unsigned j = 0; j < size; j++) {
			temp[j][j] -= ownVal[2];
		}

		temp = temp.getStepForm();

		x2 = -temp[1][2] * second / temp[1][1];
		x1 = -(temp[0][1]*x2 + temp[0][2]*second) / temp[0][0];

		result[0][2] = x1;
		result[1][2] = x2;
		result[2][2] = second;

		result.roud();
		return result;
	}

	// ��������� 2 �������
	if (ownVal[1] == ownVal[2]) {
		for (unsigned j = 0; j < size; j++) {
			temp[j][j] -= ownVal[0];
		}

		temp = temp.getStepForm();

		x2 = -temp[1][2] * second / temp[1][1];
		x1 = -(temp[0][1]*x2 + temp[0][2] * second) / temp[0][0];

		result[0][0] = x1;
		result[1][0] = x2;
		result[2][0] = second;

		/////////////////////////////////////

		temp = *this;

		for (unsigned j = 0; j < size; j++) {
			temp[j][j] -= ownVal[1];
		}

		temp = temp.getStepForm();

		result[0][1] = -temp[0][1] * first / temp[0][0];
		result[1][1] = first;
		result[2][1] = 0;

		result[0][2] = -temp[0][2] * first / temp[0][0];;
		result[1][2] = 0;
		result[2][2] = second;

		result.roud();
		return result;
	}
}

template <class T> void QubicMatrix <T>::roud() {
	for (unsigned i = 0; i < size; i++) {
		for (unsigned j = 0; j < size; j++) {
			if (abs(matrix[i][j]) < eps) {
				matrix[i][j] = 0;
			}
		}
	}
}