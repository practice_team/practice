#pragma once
#include <iostream>
#include "exception.h"
#include "trinomial.h"
#include "matrix.h"

template <class T> class QubicMatrix {
protected:
	T** matrix;

	const unsigned size = 3;
	const double   eps  = 0.00000000001;
public:
	QubicMatrix(T**);
	QubicMatrix();
	QubicMatrix(const QubicMatrix&);

	~QubicMatrix();

	T             determinant();
	Matrix <T>    subMatrix(unsigned rowIndex, unsigned columnIndex);
	Trinomial <T> �haracteristicEquation();                        
	int           ownValues(T* conatainer);                        
	QubicMatrix   getStepForm();
	unsigned      rank();
	QubicMatrix   getOwnVectors(T first = 1, T second = 1); // first,second - ������������ ����� ������������ ���������
	void          roud();

	T*           operator [] (unsigned    );
	QubicMatrix  operator +  (QubicMatrix&);
	QubicMatrix  operator *  (QubicMatrix&);
	QubicMatrix  operator *  (T           );
	QubicMatrix& operator =  (QubicMatrix&);
	QubicMatrix  operator !  (            );  // �������� ��������� ����������������� �������
	QubicMatrix  operator ~  (            );  // �������� ��������� �������� �������

	template <class T> friend QubicMatrix <T> operator *  (T            , QubicMatrix<T>&);
	template <class T> friend std::ostream&   operator << (std::ostream&, QubicMatrix<T>&);
	template <class T> friend std::istream&   operator >> (std::istream&, QubicMatrix<T>&);
};