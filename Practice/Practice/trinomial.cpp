#include "trinomial.h"
#include "common.h"

template <class T> Trinomial <T>::Trinomial(T x0) {
	coeff.push_back(x0);
	coeff.push_back(0);
	coeff.push_back(0);
	coeff.push_back(0);
}

template <class T> Trinomial <T>::Trinomial(T x0, T x1) {
	coeff.push_back(x0);
	coeff.push_back(x1);
	coeff.push_back(0);
	coeff.push_back(0);
}

template <class T> Trinomial <T>::Trinomial(T x0, T x1, T x2) {
	coeff.push_back(x0);
	coeff.push_back(x1);
	coeff.push_back(x2);
	coeff.push_back(0);
}

template <class T> Trinomial <T>::Trinomial(T x0, T x1, T x2, T x3) {
	coeff.push_back(x0);
	coeff.push_back(x1);
	coeff.push_back(x2);
	coeff.push_back(x3);
}

template <class T> Trinomial <T>::Trinomial(T* arg, unsigned count) {
	for (unsigned i = 0; i < count; i++) {
		coeff.push_back(arg[i]);
	}
}

template <class T> T Trinomial <T>::value(T x) const {
	T cur = 1, res = 0;
	for (unsigned i = 0; i < coeff.size(); i++) {
		res += cur * coeff[i];
		cur *= x;
	}
	return res;
}

template <class T> void Trinomial <T>::balance() {
	unsigned x;

	for (x = coeff.size() - 1; x > 0; x--) {
		if (abs(coeff[x] - T(0)) >= eps) {
			break;
		}
	}

	coeff.resize(x + 1);

	if (coeff.size() == 0) {
		coeff.push_back(0);
	}
}

template <class T> unsigned int Trinomial <T>::degree() const {
	return coeff.size() - 1;
}

template <class T> unsigned int Trinomial <T>::size() const {
	return coeff.size();
}

template <class T> T& Trinomial <T>::operator [] (const unsigned arg) {
	return coeff[arg];
}

template <class T> T Trinomial <T>::operator [] (const unsigned arg) const{
	return coeff[arg];
}

template <class T> Trinomial <T>& Trinomial <T>::operator = (const Trinomial <T>& arg) {
	coeff.resize(arg.coeff.size());

	for (unsigned i = 0; i < coeff.size(); i++) {
		coeff[i] = arg.coeff[i];
	}

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T>::operator = (const T& arg) {
	coeff.resize(1);
	coeff[0] = arg;

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T>::operator += (const Trinomial <T>& arg) {
	unsigned m = min(coeff.size(), arg.coeff.size());

	for (unsigned i = 0; i < m; i++) {
		coeff[i] += arg.coeff[i];
	}

	for (unsigned i = m; i < arg.coeff.size(); i++) {
		coeff.push_back(arg.coeff[i]);
	}

	balance();

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T>::operator += (const T& arg) {
	coeff[0] += arg;

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T>::operator -= (const Trinomial <T>& arg) {
	unsigned m = min(coeff.size(), arg.coeff.size());

	for (unsigned i = 0; i < m; i++) {
		coeff[i] -= arg.coeff[i];
	}

	for (unsigned i = m; i < arg.coeff.size(); i++) {
		coeff.push_back(-arg.coeff[i]);
	}

	balance();

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T>::operator -= (const T& arg) {
	coeff[0] -= arg;

	balance();

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T> ::operator *= (const Trinomial <T>& arg) {
	int old_size = coeff.size();

	coeff.resize(old_size + arg.coeff.size() + 1);

	for (int i = coeff.size() - 1; i >= old_size; i--) {
		coeff[i] = 0;
	}

	for (int i = old_size - 1; i >= 0; i--) {
		for (int j = arg.coeff.size() - 1; j > 0; j--) {
			coeff[i + j] += coeff[i] * arg.coeff[j];
		}
		coeff[i] = coeff[i] * arg.coeff[0];
	}

	balance();

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T> ::operator *= (const T& arg) {
	coeff[0] *= arg;

	balance();

	return *this;
}

template <class T> Trinomial <T>& Trinomial <T>::minus_offset(const Trinomial <T>& arg) {
	if (arg.coeff.size() > coeff.size()) {
		return *this;
	}

	for (int i = 1; i <= arg.coeff.size(); i++) {
		coeff[coeff.size() - i] -= arg.coeff[arg.coeff.size() - i];
	}

	balance();

	return *this;
}


template <class T> Trinomial <T>  operator - (const Trinomial <T>& arg) {
	Trinomial <T>  res;
	res.coeff.resize(arg.coeff.size());

	for (int i = 0; i < res.coeff.size(); i++) {
		res.coeff[i] = -arg.coeff[i];
	}

	return res;
}


template <class T> std::ostream& operator << (std::ostream& out, const Trinomial <T>& arg) {
	out << "{";
	for (unsigned i = 0; i < arg.size() - 1; i++) {
		out << arg[i] << ",";
	}

	out << arg[arg.size() - 1] << "}";

	return out;
}

template <class T> std::istream& operator >> (std::istream& in, Trinomial <T>& arg) {
	char c;

	do {
		in >> c;
	} while (isspace(c));

	if (c != '{') {
		in.exceptions(std::istream::badbit);
		return in;
	}

	arg.coeff.clear();
	for (int i = 0; true; i++) {
		T x;
		in >> x;

		arg.coeff.push_back(x);

		do {
			in >> c;
		} while (isspace(c));

		if (c == '}') {
			break;
		}

		if (c != ',') {
			in.exceptions(std::istream::badbit);
			return in;
		}
	}

	arg.balance();
	return in;
}


template <class T> Trinomial <T>  operator+ (const Trinomial <T>& a, const Trinomial <T>& b) {
	Trinomial <T>  c = a;
	c += b;
	return c;
}

template <class T> Trinomial <T>  operator+ (const Trinomial <T>& a, const T& b) {
	Trinomial <T>  c = a;
	c += b;
	return c;
}

template <class T> Trinomial <T>  operator+ (const T& a, const Trinomial <T>& b) {
	Trinomial <T>  c = b;
	c += a;
	return c;
}

template <class T> Trinomial <T>  operator- (const Trinomial <T>& a, const Trinomial <T>& b) {
	Trinomial <T>  c = a;
	c -= b;
	return c;
}

template <class T> Trinomial <T>  operator- (const Trinomial <T>& a, const T& b) {
	Trinomial <T>  c = a;
	c -= b;
	return c;
}

template <class T> Trinomial <T>  operator- (const T& a, const Trinomial <T>& b) {
	Trinomial <T>  c = b;
	c -= a;
	return c;
}

template <class T> Trinomial <T>  operator* (const Trinomial <T>& a, const Trinomial <T>& b) {
	Trinomial <T>  c = a;
	c *= b;
	return c;
}

template <class T> Trinomial <T>  operator* (const Trinomial <T>& a, const T& b) {
	Trinomial <T>  c = a;
	c *= b;
	return c;
}

template <class T> Trinomial <T>  operator* (const T& a, const Trinomial <T>& b) {
	Trinomial <T>  c = b;
	c *= a;
	return c;
}

template <class T> int Trinomial <T>::solve(T *x) const {
	T a = coeff[2] / coeff[3];
	T b = coeff[1] / coeff[3];
	T c = coeff[0] / coeff[3];

	T a2 = a*a;
	T q = (a2 - 3 * b) / 9;
	T r = (a*(2 * a2 - 9 * b) + 27 * c) / 54;
	T r2 = r*r;
	T q3 = q*q*q;

	T A, B;

	if (r2 < q3) {
		T t = r / sqrt(q3);

		if (t < -1) {
			t = -1;
		}

		if (t > 1) {
			t = 1;
		}

		t = acos(t);

		a /= 3;
		q = -2 * sqrt(q);

		x[0] = q*cos(t / 3) - a;
		x[1] = q*cos((t + TwoPi) / 3) - a;
		x[2] = q*cos((t - TwoPi) / 3) - a;

		bubble_sort(x, 3);
		roud_array(x, 3, eps);

		return 3;

	}
	else {
		A = -pow(fabs(r) + sqrt(r2 - q3), 1.0 / 3);
		if (r < 0) {
			A = -A;
		}
		B = (A == 0) ? 0 : q / A;

		a /= 3;
		x[0] = (A + B) - a;
		x[1] = -0.5*(A + B) - a;
		x[2] = 0.5*sqrt(3.)*(A - B);

		if (abs(x[2]) < SolveEps) {
			x[2] = x[1];
			bubble_sort(x, 3);
			return 2;
		}

		return 1;
	}
}