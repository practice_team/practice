#pragma once

template <typename T> T inline abs(T a) {
	return a > 0 ? a : -a;
}

template <typename T> void bubble_sort(T* a, unsigned length) {
	for (unsigned i = 0; i < length - 1; i++) {
		bool swapped = false;
		for (unsigned j = 0; j < length - i - 1; j++) {
			if (a[j] < a[j + 1]) {
				T b = a[j];
				a[j] = a[j + 1];
				a[j + 1] = b;
				swapped = true;
			}
		}

		if (!swapped) {
			break;
		}
	}
}

template <typename T> void roud_array(T* a, unsigned length, double eps) {
	for (unsigned i = 0; i < length; i++) {
		if (abs(a[i]) < eps) {
			a[i] = 0;
		}
	}
}

template <typename T> void roud_matrix(T** a, unsigned width, unsigned height, double eps) {
	for (unsigned i = 0; i < height; i++) {
		for (unsigned j = 0; j < width; j++) {
			if (abs(a[i][j]) < eps) {
				a[i][j] = 0;
			}
		}
	}
}