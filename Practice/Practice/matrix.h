#pragma once
#include <iostream>

template <class T> class Matrix
{
protected:
	T**      matrix;
	unsigned rowCount;
	unsigned columnCount;

	const double eps = 0.00000000001;

public:
	Matrix(int _rowCout, int _columnCount);
	Matrix();
	Matrix(const Matrix&);

	~Matrix();

	T        determinant();
	void     swapRows(unsigned, unsigned);
	Matrix   subMatrix(unsigned rowIndex, unsigned columnIndex); // ����� ��������� ����������, ���������� ������������� ����� ������ � ������ �������
	Matrix   getStepForm();
	unsigned rank();
	void     roud();


	T*      operator [] (int    );
	Matrix  operator +  (Matrix&);
	Matrix  operator *  (Matrix&);
	Matrix  operator *  (T      );
	Matrix& operator =  (Matrix&);
	Matrix  operator !  (       );  // �������� ��������� ����������������� �������
	Matrix  operator ~  (       );  // �������� ��������� �������� �������

	template <class T> friend Matrix <T> operator *  (T       , Matrix<T>&);
	template <class T> friend std::ostream&   operator << (std::ostream&, Matrix<T>&);
	template <class T> friend std::istream&   operator >> (std::istream&, Matrix<T>&);
};