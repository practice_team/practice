#include "slau.h"
#include "exception.h"

// ����������� ������ Slau � �������� �����������
// ��������� _equationCount � ����������� _variableCount
template <class T> Slau <T>::Slau(unsigned _equationCount, unsigned _variableCount) :
	coefficients(_equationCount, _variableCount),
	rightPart   (1             , _equationCount), 
	solution    (1             , _variableCount)
{
	equationCount = _equationCount;
	variableCount = _variableCount;
	reoder = new unsigned[variableCount];

	for (unsigned i = 0; i < variableCount; i++) {
		reoder[i] = i;
	}
}

template <class T> Slau <T>::Slau(unsigned _equationCount, unsigned _variableCount, T** matrix) :
	coefficients(_equationCount, _variableCount),
	rightPart(1, _equationCount),
	solution(1, _variableCount)
{
	for (unsigned i = 0; i < _equationCount; i++) {
		for (unsigned j = 0; j < _variableCount; j++) {
			coefficients[i][j] = matrix[i][j];
		}
	}

	for (unsigned i = 0; i < _equationCount; i++) {
		rightPart[0][i] = matrix[i][_variableCount];
	}

	equationCount = _equationCount;
	variableCount = _variableCount;
	reoder = new unsigned[variableCount];

	for (unsigned i = 0; i < variableCount; i++) {
		reoder[i] = i;
	}

}

// ������ �������������� ��������� ����� ����
template <class T> std::istream& operator >> (std::istream& in, Slau <T>& ob) {
	cout << "The coefficient matrix: ";
	in >> ob.coefficients;
	
	cout << "The vector of free terms : ";
	in >> ob.rightPart;
	
	return in;
}

// ������� ������ ������� ����
template <class T> void Slau <T>::PrintSolution(std::ostream& out) {
	if (!isSolved) {
		out << "The system is inconsistent" << endl;
		return;
	}

	if (rang < variableCount) {
		for (unsigned i = 0; i < rang; i++) {
			out << "x" << (reoder[i] + 1);
			out << " = " << solution[i][0];

			for (unsigned j = 1; j <= variableCount - rang; j++) {
				if (solution[i][j] == 0.0) {
					continue;
				}

				if (solution[i][j] > 0.0) {
					out << "+" << solution[i][j] << "*x";
					out << (reoder[rang + j - 1] + 1);
				}
				else {
					out << solution[i][j] << "*x";
					out << (reoder[rang + j - 1] + 1);
				}
			}

			out << endl;
		}
	}
	else {
		out << "(";

		for (unsigned i = 0; i < variableCount - 1; i++) {
			out << solution[0][i] << ", ";
		}

		out << solution[0][variableCount - 1] << ")" << endl;
	}
}

// ������ �������������� ��������� ������ ����
template <class T> std::ostream& operator <<(std::ostream& out, Slau <T>& ob) {
	for (unsigned i = 0; i < ob.equationCount; i++) {
		for (unsigned j = 0; j < ob.variableCount; j++) {
			out << ob.coefficients[i][j] << "\t";
		}
		out << "\t" << ob.rightPart[0][i];

		out << endl;
	}
	
	try {
		out << "Solution: " << endl;
		ob.PrintSolution(out);
	}
	catch (Exception& e) {
		e.ShowMessage();
	}

	return out;
}

// ������� ������� ���� ������� �������
template <class T> void Slau <T>::Kramer() {
	if (equationCount != variableCount) {
		throw NonSquareMatrixException();
	}
		
	T det = coefficients.determinant();

	if (det == 0.0) {
		throw ZeroDivideException();
	}

	rang = equationCount;
	Matrix <T> temp = coefficients;

	for (unsigned j = 0; j < variableCount; j++) {
		for (unsigned i = 0; i < variableCount; i++) {
			temp[i][j] = rightPart[0][i];
		}

		solution[0][j] = temp.determinant() / det;

		for (unsigned i = 0; i < variableCount; i++) {
			temp[i][j] = coefficients[i][j];
		}
	}

	isSolved = true;
}

// ������� ������� ���� � ������� �������� �������
template <class T> void Slau <T>::InverseMatrix() {
	if (equationCount != variableCount) {
		throw NonSquareMatrixException();
	}

	Matrix <T> obr = ~coefficients;
	Matrix <T> rightPart = !(this->rightPart);
	solution = obr * rightPart;
	solution = !solution;
	rang = equationCount;
	isSolved = true;
}

// ������� ������� ���� ������� �������-������
template <class T> void Slau <T>::JordanGauss() {
	Matrix <T> A = coefficients;
	Matrix <T> B = rightPart;
	bool code = true;
	unsigned count_null_cols = 0;
	
	for (unsigned i = 0; i < equationCount; i++) {
		if (A[i][i] != 0.0) {
			for (unsigned k = 0; k < equationCount; k++){
				if (k == i) {
					continue;
				}

				T d = A[k][i] / A[i][i];

				for (unsigned j = i; j < variableCount; j++) {
					A[k][j] = A[k][j] - d * A[i][j];
				}

				B[0][k] = B[0][k] - d * B[0][i];
			}

			for (unsigned j = i + 1; j < variableCount; j++) {
				A[i][j] = A[i][j] / A[i][i];
			}

			B[0][i] = B[0][i] / A[i][i];
			A[i][i] = 1;
		}
		else {
			unsigned k;
			for (k = i + 1; k < equationCount; k++) {
				if (A[k][i] != 0.0) {
					break;
				}
			}
			if (k == equationCount) {
				if(i == variableCount - 1 - count_null_cols) {
					count_null_cols++;
					code = false;
					break;
				}
				
				for (unsigned j = 0; j < equationCount; j++) {
					T t = A[j][i];
					A[j][i] =
						A[j][variableCount - count_null_cols - 1];
					A[j][variableCount - count_null_cols - 1] = t;
				}

				unsigned t = reoder[i];
				reoder[i] = reoder[variableCount - count_null_cols - 1];
				reoder[variableCount - count_null_cols - 1] = t;
				count_null_cols++;
				i--;
			}
			else {
				A.swapRows(i, k);
				T p = B[0][i];
				B[0][i] = B[0][k];
				B[0][k] = p;
				i--;
			}
		}
	}

	rang = (equationCount < variableCount - count_null_cols)
		? equationCount 
		: variableCount - count_null_cols;

	unsigned null_rows = equationCount - rang;

	for (unsigned i = rang; i < equationCount; i++) {
		if (B[0][i] != 0.0) {
			isSolved = false;
			return;
		}
	}

	Matrix <T> res(rang, 1 + variableCount - rang);

	for (unsigned i = 0; i < rang; i++) {
		res[i][0] = B[0][i];
		for (unsigned j = rang; j < variableCount; j++) {
			res[i][j - rang + 1] = -A[i][j];
		}
	}

	solution = res;
	isSolved = true;
}

// ������� ������� ����
template <class T> void Slau <T>::Solve() {
	if (equationCount == variableCount) {
		try {
				Kramer();
		}
		catch (ZeroDivideException) {
			JordanGauss();
		}
	}
	else {
		JordanGauss();
	}
}