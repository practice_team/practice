#include "exception.h"
#include "matrix.h"

template <class T> Matrix <T>::Matrix(int _rowCout, int _columnCount) {
	if (_columnCount <= 0 || _rowCout <= 0) {
		throw BadDimensionException();
	}

	columnCount = _columnCount;
	rowCount = _rowCout;

	matrix = new T*[rowCount];

	for (unsigned i = 0; i < rowCount; i++) {
		matrix[i] = new T[columnCount];
	}

	for (unsigned i = 0; i < rowCount; i++) {
		for (unsigned j = 0; j < columnCount; j++) {
			matrix[i][j] = 0;
		}
	}

}

template <class T> Matrix <T>::Matrix() {
	columnCount = rowCount = 0;
	matrix = NULL;
}

template <class T> Matrix <T>::Matrix(const Matrix <T>& ob) {
	columnCount = ob.columnCount;
	rowCount = ob.rowCount;

	if (ob.matrix == NULL) {
		matrix = NULL;
		return;
	}

	matrix = new T*[rowCount];

	for (unsigned i = 0; i < rowCount; i++) {
		matrix[i] = new T[columnCount];
	}

	for (unsigned i = 0; i < rowCount; i++) {
		for (unsigned j = 0; j < columnCount; j++) {
			matrix[i][j] = ob.matrix[i][j];
		}
	}

}

template <class T> Matrix <T>::~Matrix() {
	if (matrix != NULL) {
		for (unsigned i = 0; i < rowCount; i++) {
			delete[] matrix[i];
		}

		delete[] matrix;
	}
}

template <class T> std::istream& operator >> (std::istream& in, Matrix <T>& ob) {
	if (ob.matrix != NULL) {
		for (unsigned i = 0; i < ob.rowCount; i++) {
			for (unsigned j = 0; j < ob.columnCount; j++) {
				in >> ob[i][j];
			}
		}
	}

	return in;
}

template <class T> std::ostream& operator << (std::ostream& out, Matrix <T>& ob) {
	if (ob.matrix != NULL) {
		for (unsigned i = 0; i < ob.rowCount; i++)
		{
			for (unsigned j = 0; j < ob.columnCount; j++) {
				out << ob[i][j] << "\t";
			}
			out << endl;
		}
	}
	else {
		out << "Empty matrix" << endl;
	}

	return out;
}

template <class T> T* Matrix <T>::operator [] (int i) {
	return matrix[i];
}

template <class T> Matrix <T> Matrix <T>::operator + (Matrix <T>& ob) {
	if (ob.columnCount == columnCount && ob.rowCount == rowCount) {
		Matrix <T> temp(rowCount, columnCount);

		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < columnCount; j++) {
				temp.matrix[i][j] = matrix[i][j] + ob.matrix[i][j];
			}
		}

		return temp;
	}
	else {
		throw DimensionSumException();
	}
}

template <class T> Matrix <T> Matrix <T>::operator * (Matrix <T>& ob) {
	if (columnCount == ob.rowCount) {
		Matrix <T> temp(rowCount, ob.columnCount);

		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < ob.columnCount; j++) {
				for (int k = 0; k < columnCount; k++) {
					temp.matrix[i][j] =
						temp.matrix[i][j] + matrix[i][k] * ob.matrix[k][j];
				}

			}

		}

		return temp;
	}
	else {
		throw DimensionProductException();
	}
}

template <class T> Matrix <T> Matrix <T>::operator * (T number) {
	Matrix <T> temp(rowCount, columnCount);

	for (int i = 0; i < rowCount; i++) {
		for (int j = 0; j < columnCount; j++) {
			temp[i][j] = matrix[i][j] * number;
		}
	}

	return temp;
}

template <class T> Matrix <T> operator * (T number, Matrix <T>& ob) {
	Matrix <T> temp(ob.rowCount, ob.columnCount);

	for (int i = 0; i < ob.rowCount; i++) {
		for (int j = 0; j < ob.columnCount; j++) {
			temp[i][j] = number * ob[i][j];
		}
	}

	return temp;
}

template <class T> Matrix <T>& Matrix <T>::operator = (Matrix <T>& ob) {
	if (columnCount != ob.columnCount || rowCount != ob.rowCount) {
		for (unsigned i = 0; i < rowCount; i++) {
			delete[] matrix[i];
		}

		delete[] matrix;

		columnCount = ob.columnCount;
		rowCount = ob.rowCount;
		matrix = new T*[rowCount];

		for (unsigned i = 0; i < rowCount; i++) {
			matrix[i] = new T[columnCount];
		}
	}

	for (unsigned i = 0; i < rowCount; i++) {
		for (unsigned j = 0; j < columnCount; j++) {
			matrix[i][j] = ob.matrix[i][j];
		}
	}

	return *this;
}

template <class T> Matrix <T> Matrix <T>::operator ! () {
	Matrix <T> temp(columnCount, rowCount);

	for (int i = 0; i < rowCount; i++) {
		for (int j = 0; j < columnCount; j++) {
			temp.matrix[j][i] = matrix[i][j];
		}
	}

	return temp;
}

template <class T> Matrix <T> Matrix <T>::subMatrix(unsigned rowIndex, unsigned columnIndex) {
	Matrix <T> temp(rowCount - 1, columnCount - 1);

	for (unsigned i = 0; i < rowIndex; i++) {
		for (unsigned j = 0; j < columnIndex; j++) {
			temp.matrix[i][j] = matrix[i][j];
		}

		for (unsigned j = columnIndex + 1; j < columnCount; j++) {
			temp.matrix[i][j - 1] = matrix[i][j];
		}
	}

	for (unsigned i = rowIndex + 1; i < rowCount; i++) {
		for (unsigned j = 0; j < columnIndex; j++) {
			temp.matrix[i - 1][j] = matrix[i][j];
		}

		for (unsigned j = columnIndex + 1; j < columnCount; j++) {
			temp.matrix[i - 1][j - 1] = matrix[i][j];
		}
	}

	return temp;
}

template <class T> T Matrix <T>::determinant() {
	T det = 0;

	if (rowCount != columnCount) {
		throw NonSquareMatrixException();
	}

	if (columnCount == 1) {
		return matrix[0][0];
	}

	Matrix <T> temp(rowCount - 1, columnCount - 1);

	for (unsigned j = 0; j < columnCount; j++) {
		temp = subMatrix(0, j);

		if (j % 2 == 0) {
			det = det + temp.determinant() * matrix[0][j];
		}
		else {
			det = det - temp.determinant() * matrix[0][j];
		}
	}

	return det;
}

template <class T> void Matrix <T>::swapRows(unsigned index1, unsigned index2) {
	if (index1 < 0 || index2 < 0 || index1 >= rowCount || index2 >= rowCount) {
		return;
	}

	for (unsigned i = 0; i < columnCount; ++i) {
		std::swap(matrix[index1][i], matrix[index2][i]);
	}
}

template <class T> Matrix <T> Matrix <T>::operator ~ () {
	if (rowCount != columnCount) {
		throw NonSquareMatrixException();
	}

	Matrix <T> res(columnCount, columnCount);
	T det = determinant();

	if (det == 0.0) {
		throw ZeroDivideException();
	}

	Matrix <T> temp(columnCount - 1, columnCount - 1);
	int z;

	for (int i = 0; i < columnCount; i++) {
		z = (i % 2 == 0) ? 1 : -1;

		for (int j = 0; j < columnCount; j++) {
			temp = subMatrix(i, j);
			res[j][i] = z * temp.determinant() / det;
			z = -z;
		}
	}

	return res;
}

template <class T> Matrix <T> Matrix <T>::getStepForm() {
	Matrix <T> temp = *this;
	unsigned i, j, k;
	unsigned iMax;

	for (i = 0; i < rowCount - 1; i++) {
		// ������� ������ � ������������ ������ ���������
		iMax = i;
		for (j = i + 1; j < rowCount; j++) {
			if (abs(temp[j][i]) > abs(temp[iMax][i])) {
				iMax = j;
			}
		}

		if (abs(temp[iMax][i]) < eps) {
			continue;
		}

		for (k = 0; k < columnCount; k++) {
			std::swap(temp[i][k], temp[iMax][k]);
		}

		//  �������� ������� ������ �� ���� ���������
		for (j = i + 1; j < rowCount; j++) {
			T q = -temp[j][i] / temp[i][i];
			for (k = i; k < rowCount; k++) {
				temp[j][k] += q * temp[i][k];
			}
		}
	}

	return temp;
}

template <class T> unsigned Matrix <T>::rank(){
	Matrix <T> temp = getStepForm();
	unsigned result = 0;

	unsigned n = (rowCount < columnCount) ? rowCount : columnCount;

	for (unsigned i = 0; i < n; i++) {
		if (temp[i][i] != 0) {
			result++;
		}
	}

	return result;
}

template <class T> void Matrix <T>::roud() {
	for (unsigned i = 0; i < rowCount; i++) {
		for (unsigned j = 0; j < columnCount; j++) {
			if (abs(matrix[i][j]) < eps) {
				matrix[i][j] = 0;
			}
		}
	}
}